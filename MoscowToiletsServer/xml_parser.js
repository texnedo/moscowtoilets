var libxmljs = require("libxmljs");
var fs = require('fs');

var MongoClient = require('mongodb').MongoClient;
var format = require('util').format;    

/*people_choice_toilets_geo.xml*/

MongoClient.connect('mongodb://127.0.0.1:27017/objects', function(err, db) {
	if(err) throw err;
	fs.readFile('people_choice_toilets_geo.xml', function (err, data) {
		var xmlDoc = libxmljs.parseXml(data);
		var collection = db.collection('toilet_objects');

		// xpath queries
		var gchild = xmlDoc.get('//data//items').childNodes();
		ProcessToilet(collection, gchild, 1, function(){
			console.log('Save completed');
			db.close();
		});
	});

});

function ProcessToilet(collection, childs, index, callback){
	if (index > childs.length)
	{
		callback();
		return;
	}

	console.log('Start to save item with index = ' + index.toString());
	var toilet = new Object();
		
	try
	{
		var address = childs[index].attr('addr').value();
		var lon = childs[index].attr('lon').value();
		var lat = childs[index].attr('lat').value();
	}
	catch(error){}

	if (address == null)
	{
		ProcessToilet(collection, childs, ++index, callback);
		return;
	}

	toilet.source = "obossus.ru";
	toilet.loc = [parseFloat(lon), parseFloat(lat)];

	toilet.has_preview_100_100 = false;
	toilet.has_preview_200_200 = false;
	toilet.user_address = address;

	collection.insert(toilet, function(err, db){
		if(err)
		{
			console.log('Failed to save new toilet object into db');
			callback();
		}
		else
		{
			console.log('Saved item with index = ' + index.toString());
			ProcessToilet(collection, childs, ++index, callback);
		}
	});
}

/*moscow_toilets_geo.xml*/

/*MongoClient.connect('mongodb://127.0.0.1:27017/objects', function(err, db) {
	if(err) throw err;
	fs.readFile('moscow_toilets_geo.xml', function (err, data) {
		var xmlDoc = libxmljs.parseXml(data);
		var collection = db.collection('toilet_objects');

		// xpath queries
		var gchild = xmlDoc.get('//data//items').childNodes();
		ProcessToilet(collection, gchild, 1, function(){
			console.log('Save completed');
			db.close();
		});
	});

});

function ProcessToilet(collection, childs, index, callback){
	if (index > childs.length)
	{
		callback();
		return;
	}

	console.log('Start to save item with index = ' + index.toString());
	var toilet = new Object();
		
	try
	{
		var address = childs[index].attr('addr').value();
		var lon = childs[index].attr('lon').value();
		var lat = childs[index].attr('lat').value();
		var code = childs[index].attr('code').value();
		var time = childs[index].attr('time').value();
		var availability = childs[index].attr('avail').value();
	}
	catch(error){}

	if (address == null)
	{
		ProcessToilet(collection, childs, ++index, callback);
		return;
	}

	if (availability != null && availability.indexOf("Приспособлен") != -1)
		toilet.availability = true;

	if (time != null && time.indexOf("Временно закрыт") != -1)
		toilet.isopen = false;

	if (time != null && time.split("С").length == 2)
	{
		var from = time.split("С")[1].split("до")[0].trim();
		var to = time.split("С")[1].split("до")[1].trim();

		toilet.open_from = from;  
		toilet.open_to = to;
	}

	toilet.code = code;
	toilet.source = "dt.mos.ru";
	toilet.loc = [parseFloat(lon), parseFloat(lat)];

	toilet.has_preview_100_100 = false;
	toilet.has_preview_200_200 = false;
	toilet.user_address = address;

	//console.log(toilet);
	//ProcessToilet(collection, childs, ++index, callback);
	collection.insert(toilet, function(err, db){
		if(err)
		{
			console.log('Failed to save new toilet object into db');
			callback();
		}
		else
		{
			console.log('Saved item with index = ' + index.toString());
			ProcessToilet(collection, childs, ++index, callback);
		}
	});
}*/