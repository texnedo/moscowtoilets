var http = require('http');

http.createServer(function (request, response) {
	console.log('Connection detected on 8124');

	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end('Hello World\n');
}).listen(8124);

console.log('Server running at http://127.0.0.1:8124/');
console.log('Server running at http://127.0.0.1:8124/ Max connections = ' +  http.globalAgent.maxSockets.toString());

for (var i = 0; i < 20000; i++) {
	
	var options = {
	  hostname: '127.0.0.1',
	  port: 8124,
	  path: '/',
	  method: 'GET',
	  agent:false
	};

	var req = http.request(options, function(res) {
	  console.log('STATUS: ' + res.statusCode);
	  console.log('HEADERS: ' + JSON.stringify(res.headers));
	  res.setEncoding('utf8');
	  res.on('data', function (chunk) {
	    console.log('BODY: ' + chunk);
	  });
	});

	req.on('error', function(e) {
	  console.log('problem with request: ' + e.message);
	});

	// write data to request body
	req.end();
};
