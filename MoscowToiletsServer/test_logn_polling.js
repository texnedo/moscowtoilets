var http = require('http');

http.createServer(function (request, response) {
	console.log('Connection detected on 80 port');

	console.log('Start timer');
	setTimeout(function(){
		console.log('Time elapsed');		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Hello World\n');
	}, 10000000);
}).listen(80);

console.log('Server running at http://127.0.0.1:80/');
console.log('Server running at http://127.0.0.1:80/ Max connections = ' +  http.globalAgent.maxSockets.toString());
