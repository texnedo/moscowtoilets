var http = require('http');
var fs = require('fs');

/*
    lat = 51.23445
    lon = 34.12312
    description = Toilet description
    open_from = 10.00 (Start work time, in 24H format)
    open_to = 24.00 (End work time, in 24H format)
    address = Full object address
    price = Toilet enterance price
    accessibility = true/false
*/

var options = {
  hostname: '127.0.0.1',
  port: 80,
  path: '/createObject?lat=51.234451&lon=34.12312&description=sometext&open_from=10.00&open_to=24.00&address=someaddress&price=25&accessibility=false',
  agent:false,
  method: 'POST'
};

var optionsUpload = {
  hostname: '127.0.0.1',
  port: 80,
  path: '/uploadImage?name=some.jpg&size=213234&upload_token=',
  agent:false,
  method: 'POST'
};


var req = http.request(options, function(res) {
  console.log('STATUS: ' + res.statusCode);
  console.log('HEADERS: ' + JSON.stringify(res.headers));
  res.on('data', function (chunk) {
    console.log('BODY: ' + chunk);

    var answer = JSON.parse(chunk);
    console.log('answer: ' + answer.result.upload_token);
    optionsUpload.path += answer.result.upload_token;

      var reqUpload = http.request(optionsUpload, function(resUpload){
      resUpload.on('data', function (chunk2) {
        console.log('BODY2: ' + chunk2);
      });

      reqUpload.on('error', function(e) {
        console.log('problem with request: ' + e.message);
      });
    });

    reqUpload.setHeader("Content-Type", "application/octet-stream");
    fs.readFile('sample_image.jpg', function (err, data) {
      console.log('Read file chunk');
      if (err) 
        throw err;
      reqUpload.write(data);
      reqUpload.end();
    });

      });
});

req.on('error', function(e) {
  console.log('problem with request: ' + e.message);
});

req.end();

/*req.end();
req.setHeader("Content-Type", "application/octet-stream");
fs.readFile('sample_image.jpg', function (err, data) {
  console.log('Read file chunk');
  if (err) throw err;
  req.write(data);
  req.end();
});*/