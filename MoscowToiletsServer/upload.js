var http = require('http');
var dns = require('dns');
var uuid = require('uuid');
var os = require('os');
var fs = require('fs');
var url = require('url');
var querystring = require('querystring');
var mongoClient = require('mongodb').MongoClient;
var gm = require('gm');
var imageMagick = gm.subClass({ imageMagick: true });
var crc32 = require('buffer-crc32');

var LOCAL_API_PORT = 80;
var UPLOAD_CHUNK_TIMEOUT = 30000;
var MAX_DESCRIPTION_TEXT_LENGTH = 1000;
var MAX_ADDRESS_TEXT_LENGTH = 500;
var MAX_FILE_SIZE = 5 * 1024 * 1024;
var DEBUG = true;
var DATABASE_HOST = '127.0.0.1'
var DATABASE_PORT = '27017'
var DATABASE_OBJECTS = 'objects'

/*
------------Upload photo API------------

Maximum image size = 5mb
Allowed extensions = jpg, png, jpeg

Request - createObject
	HTTP request parametres:
		lat = 51.23445
		lon = 34.12312
		description = Toilet description
		open_from = 10.00 (Start work time, in 24H format)
		open_to = 24.00 (End work time, in 24H format)
		address = Full object address
		price = Toilet enterance price
		accessibility = true/false
	Returns:
		{"status":"200", "result":{"upload_token":"dhkj234h5lk2345lkjhlk"}, "description":""}
		{"status":"401", "description":"Not all mandatory parametres provided in the request"}
		{"status":"402", "description":"Wrong object location"}
		{"status":"500", "description":"Unknown"}

Request - uploadImage
	HTTP request parametres:
		upload_token = dhkj234h5lk2345lkjhlk
		name = some_image.jpg
		size = 23423 (File size, in bytes)
	Returns:
		{"status":"200", "description":"File successfully uploaded"}
		{"status":"401", "description":"File extension is not supported"}
		{"status":"402", "description":"Too large file to upload"}
		{"status":"500", "description":"Unknown"}

Request - getObjects
	HTTP request parametres:
		lat = 51.23445
		lon = 34.12312
		radius = 1000 (Radius of circle on the map where requested objects should be located, in meters)
		has_price = true/false
		accessibility = true/false
		open_from = 10.00 (Start work time, in 24H format)
		open_to = 24.00 (End work time, in 24H format)
	Returns:
		{
		    "status": "200",
		    "description":"",
		    "result":{
				"count":2
			    "items": [
			        {
			            "code": "234523452345",
			            "lat": "51.23445",
			            "lon": "34.12312",
			            "description": "Some descr",
			            "address": "Some address",
			            "price": "30",
			            "open_from": "10.00",
			            "open_to": "24.00",
			            "accessibility": true,
			            "image_100_100":"url to image",
			            "image_200_200":"url to image"
			        	"image_original":"url to image"
			        },
			        {
			            "code": "234523452345",
			            "lat": "51.23445",
			            "lon": "34.12312",
			            "description": "Some descr",
			            "address": "Some address",
			            "price": "30",
			            "open_from": "10.00",
			            "open_to": "24.00",
			            "accessibility": true,
			            "image_100_100":"url to image",
			            "image_200_200":"url to image"
			        }
			    ]
		    }
		}
		{"status":"401", "description":"Wrong query parametres provided"}
		{"status":"500", "description":"Unknown"}
*/

function RejectHttpRequest(response, code, reason){
	console.log('Reject request by reason = "' + reason + '" with code = ' + code.toString());
	response.writeHead(code, {'Content-Type': 'text/plain; charset=utf-8', 'Connection': 'close'});
	response.write('{"status_http": '+ code.toString() +', "description" : "' + reason + '"}');
	response.end();
}

function RejectRequest(response, code, reason){
	console.log('Reject request by reason = "' + reason + '" with code = ' + code.toString());
	response.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8', 'Connection': 'close'});
	response.write('{"status": '+ code.toString() +', "description" : "' + reason + '"}');
	response.end();
}

function CompleteRequestWithSuccess(response, answer){
	console.log('Complete request by success result= "' + JSON.stringify(answer) + '" with code = 200');
	response.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8', 'Connection': 'close'});
	response.write('{"status": 200, "description" : "", "result":' + JSON.stringify(answer) + '}');
	response.end();
}

function EndRequestWithSuccess(response, answer){
	console.log('Complete request by success result= "' + JSON.stringify(answer) + '" with code = 200');
	response.end();
}

function IsInRange(min, number, max){
	if (!isNaN(number) && (number >= min) && (number <= max)) 
		return (true);
    else 
    	return (false);
}

function ValidateGeoLocation (lattitude, longitude) {
	if (IsInRange(-90, lattitude, 90) && IsInRange(-180, longitude, 180)) 
    	return (true);
    else
    	return (false);
}

function ValidateTime (argument) {
	if (argument == null || argument.length == 0)
		return (false);

	var tokens = argument.split('.');

	if (tokens.length == 2 &&
		(!isNaN(tokens[0]) && tokens[0] >= 0 && tokens[0] <= 24) && 
		(!isNaN(tokens[1]) && tokens[1] >= 0 && tokens[1] <= 59)
		)
		return (true);
	else 
		return (false);
}

function ValidateBool (argument) {
	if (argument == 'true' || argument == 'false')
		return (true);
	else
		return (false);
}

function ValidateImageFileName (argument) {
	if (argument == null || argument.length == 0)
		return (null);

	var tokens = argument.split('.');

	if (tokens.length == 2 &&
		tokens[0].length > 0 && 
		(tokens[1] == 'jpg' || tokens[1] == 'png' || tokens[1] == 'jpeg') 
		)
		return (tokens[1]);
	else 
		return (null);
}

function CreateImagePreview (src, height, width, dest, callback) {
	console.log('Start create image prevew ' + height + 'X' + width + ' ' + dest);
	/*fs.link(src, dest, function(err, args){
		callback(dest);
	});*/
	imageMagick(src)
	.resize(width)
	.crop(width, height, width / 2, height / 2)
	.write(dest, function (err) {
  		if (err)
  		{
  			console.log('Image resize failed ' + height + 'X' + width + ' ' + src + ' error = ' + err);
	  		callback(null);
	  		return;
  		}

  		console.log('Created image prevew ' + height + 'X' + width + ' ' + dest);
		callback(dest);
	});
}

function ProcessCreateObject(params, db, callback){
	var hasTime = false;
	var hasPrice = false;
	var hasAccessibility = false;
	var hasDescription = false;
	var hasAddress = false;

	if (!params.hasOwnProperty("lat") || 
		!params.hasOwnProperty("lon") || 
		!ValidateGeoLocation(params.lat, params.lon))
	{
		console.log('Request has invalid location');
		callback(null);
		return;
	}

	if (params.hasOwnProperty("open_from") && 
		!ValidateTime(params.open_from))
	{
		console.log('Request has invalid work time = open_from');
		callback(null);
		return;
	}

	if (params.hasOwnProperty("open_to") && 
		!ValidateTime(params.open_to))
	{
		console.log('Request has invalid work time = open_to');
		callback(null);
		return;
	}

	if ((
			params.hasOwnProperty("open_from") && 
			!params.hasOwnProperty("open_to")
		) 
		|| 
		(
			params.hasOwnProperty("open_to") && 
			!params.hasOwnProperty("open_from")
		))
	{
		console.log('Request has invalid work time = both');
		callback(null);
		return;
	}
	else
		hasTime = true;

	if (params.hasOwnProperty("accessibility") && 
		!ValidateBool(params.accessibility))
	{
		console.log('Request has invalid accessibility');
		callback(null);
		return;
	}
	else
		hasAccessibility = true;

	if (params.hasOwnProperty("price") && 
		!IsInRange(0, params.price, Number.MAX_VALUE))
	{
		console.log('Request has invalid price');
		callback(null);
		return;
	}
	else
		hasPrice = true;

	if (params.hasOwnProperty("description") && 
		params.description.length > MAX_DESCRIPTION_TEXT_LENGTH)
	{
		console.log('Request has invalid description');
		callback(null);
		return;
	}
	else
		hasDescription = true;

	if (params.hasOwnProperty("address") && 
		params.address.length > MAX_ADDRESS_TEXT_LENGTH)
	{
		console.log('Request has invalid address');
		callback(null);
		return;
	}
	else
		hasAddress = true;


	console.log('New toilet object had been crated');

	var toilet = new Object();
	
	//create location object stored in GEOJSON format for index
	toilet.loc = [parseFloat(params.lon), parseFloat(params.lat)];

	toilet.has_preview_100_100 = false;
	toilet.has_preview_200_200 = false;

	if (hasTime) 
	{
		toilet.open_from = params.open_from;  
		toilet.open_to = params.open_to;
	}

	if (hasAccessibility)
		toilet.accessibility = params.accessibility == "true" ? true : false;

	if (hasPrice)
		toilet.price = parseFloat(params.price);

	if (hasDescription)
		toilet.description = params.description;

	if (hasAddress)
		toilet.user_address = params.address;

	toilet.upload_token = uuid.v4();
	toilet.wait_for_review = true;

	var collection = db.collection('toilet_objects');
	collection.insert(toilet, function(err, db){
		if(err)
		{
			console.log('Failed to save new toilet object into db');
			callback(null);
		}
		else
		{
			console.log('New toilet object saved into db');
			var answer = new Object();
			answer.upload_token = toilet.upload_token;
			callback(answer);
		}
	});
}

function ProcessGetObjects(params, db, callback){

}

function ProcessUploadImage(request, params, db, validated_callback, callback_completed){
	if (!params.hasOwnProperty("upload_token") || 
		!(/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(params.upload_token)))
	{
		console.log('Request has invalid upload_token');
		callback_completed(null);
		return;
	}

	var fileExt = null;
	if (!params.hasOwnProperty("name") || 
		(fileExt = ValidateImageFileName(params.name)) == null)
	{
		console.log('Request has invalid name');
		callback_completed(null);
		return;
	}

	if (!params.hasOwnProperty("size") || 
		!IsInRange(0, params.size, MAX_FILE_SIZE))
	{
		console.log('Request has invalid size');
		callback_completed(null);
		return;
	}
	
	var collection = db.collection('toilet_objects');
	collection.findOne({upload_token:params.upload_token}, function(err, item) {
		if (err ||
			item == null || 
			item.upload_token != params.upload_token)
		{
			console.log('Upload token not found in db');
			callback_completed(null);
			return;
		}

		console.log('Found object in db:\n' + JSON.stringify(item));

		validated_callback();

		//open file stream and wait when we will be ready to write into it
		var totalFileSize = 0;
		var fileNameBase = Math.abs(crc32(item.upload_token + item._id).readInt32LE(0)).toString() + '-' + item.upload_token;
		var finalFileName = fileNameBase  + '.' + fileExt;
		var finalFileName100_100 = fileNameBase  + '_100_100.' + fileExt;
		var finalFileName200_200 = fileNameBase  + '_200_200.' + fileExt;
		var fileStream = fs.createWriteStream(finalFileName, {flags: 'w'});
		fileStream.on('open', function(fd){
			console.log('File opened for writing.');

			request.on('data', function (chunk) {
		    	console.log('Client transfered new data chunk with size = ' + chunk.length.toString());
		      	fileStream.write(chunk);

		      	totalFileSize += chunk.length;
		      	if (totalFileSize > MAX_FILE_SIZE)
		      	{
					console.log('Too big file size');
					callback_completed(null);
					return;
				}
		  	});

			request.on('end', function () {
				console.log('Client finished data transfer. File size = ' + totalFileSize + ', File name = ' + finalFileName);
				fileStream.end();
				callback_completed(new Object());

				CreateImagePreview(finalFileName, 100, 100, finalFileName100_100, function(argument){
					if (argument)
					{
						collection.update({upload_token:params.upload_token}, 
										{$set:{has_preview_100_100:true}}, 
										function(err, db){
											if (err)
												console.log('Failed to save 100x100 image state for ' + params.upload_token);
											else
												console.log('100x100 image saved for ' + params.upload_token);	
										});
					}
				});

				CreateImagePreview(finalFileName, 200, 200, finalFileName200_200, function(argument){
					if (argument)
					{
						collection.update({upload_token:params.upload_token}, 
											{$set:{has_preview_200_200:true}}, 
											function(err, db){
												if (err)
													console.log('Failed to save 200x200 image state for ' + params.upload_token);
												else
													console.log('200x200 image saved for ' + params.upload_token);	
											});
					}
				});
			});
		});
  	});
}

dns.lookup(os.hostname(), function (err, address, family) {
  console.log('Upload server running at: ' + address + ':' + LOCAL_API_PORT + '\nMax connections = ' + http.globalAgent.maxSockets.toString() + '\nModeDebug = ' + DEBUG.toString());

  mongoClient.connect('mongodb://' + DATABASE_HOST + ':' + DATABASE_PORT + '/' + DATABASE_OBJECTS, function(err, db) {
    if(err){
    	console.log('Failed to connect to database on ' + DATABASE_HOST + ':' + DATABASE_PORT + '/' + DATABASE_OBJECTS);
    	return;
    }

    console.log('Server connected to database on ' + DATABASE_HOST + ':' + DATABASE_PORT + '/' + DATABASE_OBJECTS);

    http.createServer(function (request, response) {
		var urlParts = url.parse(request.url, false);
        var queryString = querystring.unescape(urlParts.query);
		
        console.log('----------------------------------------');
		console.log('Connection detected on port: ' + LOCAL_API_PORT + '\n query = ' + queryString + '\n method ' + urlParts.pathname);

        var paramsObject = querystring.parse(queryString);
		if (paramsObject == null){
			RejectHttpRequest(response, 405, "Wrong http query")
			return;
		}

		switch (urlParts.pathname)
		{
			case '/createObject':
			{
				ProcessCreateObject(paramsObject, db, function(result){
					if (result != null)
						CompleteRequestWithSuccess(response, result);
					else
						RejectRequest(response, 500, "Wrong request details found");
				});
				break;
			}
			case '/uploadImage':
			{
                request.setTimeout(UPLOAD_CHUNK_TIMEOUT);

		        if (request.method != 'POST'){
			        RejectHttpRequest(response, 405, "Wrong http method used")
			        return;
		        }

				ProcessUploadImage(
					request,
					paramsObject, 
					db, 
					function(){
						response.writeHead(200, {'Content-Type': 'application/octet-stream', 'Connection': 'close'});
					}, 
					function(result){
						if (result != null)
							EndRequestWithSuccess(response, result);
						else
							RejectRequest(response, 500, "Wrong request details found");
					}
				);
				break;
			}
			case '/getObjects':
			{
				ProcessGetObjects(paramsObject, db, function(result){
					if (result != null)
						CompleteRequestWithSuccess(response, result);
					else
						RejectRequest(response, 500, "Wrong request details found");
				});
				break;
			}
			default:
			{
				//create error header for client
				RejectHttpRequest(response, 404, "Wrong API method called");
				return;
			}
		}
	}).listen(LOCAL_API_PORT);
});

})
