﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

#if WINDOWS_STORE
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml;
#elif WINDOWS_PHONE && WP7
    using MoscowToiletsWP7;
    using MoscowToiletsWP7.ViewModel;
    using System.Windows.Media;
#elif WINDOWS_PHONE && WP8
    using MoscowToiletsWP7;
    using MoscowToiletsWP7.ViewModel;
    using MoscowToiletsWP8;
    using System.Windows.Media;
#endif

namespace MoscowToilets.ViewModel
{
    public class SettingsViewModel : ViewModelBase
    {
        public SettingsViewModel()
        {
            
        }

        public bool UseGpsLocationData
        {
            get
            {
                if (App.Settings.HasValue("SettingsViewModel.UseGpsLocationData"))
                    return ((bool) App.Settings.GetValue("SettingsViewModel.UseGpsLocationData"));
                else
                    return (true);
            }

            set
            {
                App.Settings.SetValue("SettingsViewModel.UseGpsLocationData", value);
                RaisePropertyChanged("UseGpsLocationData");
            }
        }

        public bool UseCachedPositionWhenGpsUnavailable
        {
            get
            {
                if (App.Settings.HasValue("SettingsViewModel.UseCachedPositionWhenGpsUnavailable"))
                    return ((bool)App.Settings.GetValue("SettingsViewModel.UseCachedPositionWhenGpsUnavailable"));
                else
                    return (true);
            }

            set
            {
                App.Settings.SetValue("SettingsViewModel.UseCachedPositionWhenGpsUnavailable", value);
                RaisePropertyChanged("UseCachedPositionWhenGpsUnavailable");
            }
        }

        private ICommand _privacyPolicyPageNavigateCommand;

        public ICommand PrivacyPolicyPageNavigateCommand
        {
            get
            {
                return _privacyPolicyPageNavigateCommand ??
                       (_privacyPolicyPageNavigateCommand =
                        new RelayCommand(() => ((App) Application.Current).RootFrame.NavigateToView(
                            new Uri("/View/Pages/PrivacyPolicyPage.xaml", UriKind.Relative), null)));
            }
        }
    }
}
