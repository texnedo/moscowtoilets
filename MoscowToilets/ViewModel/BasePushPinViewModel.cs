﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Yandex.Maps;
using Yandex.Positioning;

#if WINDOWS_STORE
using Windows.UI.Xaml;
#elif WP7
using MoscowToiletsWP7;
using System.Windows;
#elif WP8
using MoscowToiletsWP8;
using System.Windows;
#endif

namespace MoscowToilets.ViewModel
{
    public class BasePushPinViewModel : ViewModelBase
    {
        public virtual Visibility Visibility { get; set; }
        
        public virtual Visibility ContentVisibility { get; set; }
        
        public PushPinState State { get; set; }
        
        public virtual GeoCoordinate Position { get; set; }
        
        public int ZIndex { get; set; }
    }
}
