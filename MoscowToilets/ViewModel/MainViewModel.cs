using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MoscowToilets.Model.DataMos;
using MoscowToilets.Model.Geo;
using MoscowToilets.ViewModel.Filtering;
using Yandex.Positioning;

#if WINDOWS_STORE
    using Windows.UI.Xaml.Media;
    using Windows.UI.Xaml;
#elif WINDOWS_PHONE && WP7
    using MoscowToiletsWP7;
    using MoscowToiletsWP7.ViewModel;
    using System.Windows.Media;
#elif WINDOWS_PHONE && WP8
    using MoscowToiletsWP7;
    using MoscowToiletsWP7.ViewModel;
    using MoscowToiletsWP8;
    using System.Windows.Media;
#endif

namespace MoscowToilets.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
#if WINDOWS_PHONE
        private const int MAP_REQUEST_TIMEOUT = 800;

        private const int UNLOAD_TOILET_COLLECTION_TIMEOUT = 15000;
#endif

        public MainViewModel()
        {
            if (App.Settings.HasValue("MainViewModel.SelectedDataSource"))
            {
                _selectedDataSource =
                    (DtMosDataProvider.StorageType) App.Settings.GetValue("MainViewModel.SelectedDataSource");
            }

            if (App.Settings.HasValue("MainViewModel.AccessibilityFilter") &&
                (bool)App.Settings.GetValue("MainViewModel.AccessibilityFilter"))
            {
                _filteredData.AddFilter(FilterType.AccessibilityFilter);
            }

            if (App.Settings.HasValue("MainViewModel.TimeFilter") &&
                (bool)App.Settings.GetValue("MainViewModel.TimeFilter"))
            {
                _filteredData.AddFilter(FilterType.TimeFilter);
            }

            _filteredData.PropertyChanged += (obj, e) =>
                {
                    if (e.PropertyName == "ResultData")
                    { 
                        RaisePropertyChanged("MixedDataSource");
                        DataSourceReady = true;
#if WINDOWS_PHONE
                        UpdateMixedCollection();
#endif
                    }

                if (e.PropertyName == "IsOperationInProgress")
                    IsOperationInProgress = _filteredData.IsOperationInProgress;
            };

#if WINDOWS_PHONE

#if DEBUG
            _mixedCollection.CollectionChanged += (obj, args) =>
                {
                    Debug.WriteLine("Mixed collection changed size = " + _mixedCollection.Count().ToString());

                    if (args.NewItems != null)
                        Debug.WriteLine("new items  = " + args.NewItems.Count.ToString());

                    if (args.OldItems != null)
                        Debug.WriteLine("removed items = " + args.OldItems.Count.ToString());
                };
#endif

            _mapRequestTimer.Interval = TimeSpan.FromMilliseconds(MAP_REQUEST_TIMEOUT);
            _mapRequestTimer.Tick += (obj, args) =>
            {
                _mapRequestTimer.Stop();

                Debug.WriteLine("Timer for Map request ticked");

                Deployment.Current.Dispatcher.BeginInvoke(() => UpdateMixedCollection());
            };

            _unloadCollectionTimer.Interval = TimeSpan.FromMilliseconds(UNLOAD_TOILET_COLLECTION_TIMEOUT);
            _unloadCollectionTimer.Tick += (obj, args) =>
            {
                _unloadCollectionTimer.Stop();

                Debug.WriteLine("Unload timer for Map ticked");

                Deployment.Current.Dispatcher.BeginInvoke(() => UnloadMixedCollection());
            };
#endif
        }

        private readonly DtMosDataProvider _provider = new DtMosDataProvider();

        private readonly FilterList _filteredData = new FilterList();

        private Dictionary<DtMosDataProvider.StorageType, List<ToiletPushPinViewModel>> _dataSource = null;

#if WINDOWS_PHONE
        private readonly ToiletViewModelCollection _mixedCollection = new ToiletViewModelCollection();

        private readonly DispatcherTimer _mapRequestTimer = new DispatcherTimer();

        private readonly DispatcherTimer _unloadCollectionTimer = new DispatcherTimer();
#endif

        private UserPositionPushPinViewModel _positionPushPin = null;

        /// <summary>
        /// Default value of datasource type
        /// </summary>
        private DtMosDataProvider.StorageType _selectedDataSource = DtMosDataProvider.StorageType.OpenData |
                                                                    DtMosDataProvider.StorageType.OfficialDataMoscow;

        public List<UserPositionPushPinViewModel> PositionDataSource
        {
            get
            {
                var data = new List<UserPositionPushPinViewModel>();
                if (_positionPushPin == null)
                {
                    //TODO - ViewModel to viewmodel communication should be implemented by using message passing mechanism
                    _positionPushPin = new UserPositionPushPinViewModel();
                    RaisePropertyChanged("GeoLocationStatus");
                    RaisePropertyChanged("PositionDataSource");

                    _positionPushPin.PropertyChanged += (obj, arg) =>
                        {
                            if (arg.PropertyName == "GeoLocationStatus")
                            { 
                                RaisePropertyChanged("GeoLocationStatus");
                                RaisePropertyChanged("PositionDataSource");
                            }
                        };
                }
                data.Add(_positionPushPin);
                
                return (data);
            }
        }

        public ToiletViewModelCollection MixedCollection
        {
            get
            {
                if (_dataSource == null)
                    LoadDataAsync(_selectedDataSource);
                
                return (_mixedCollection);
            }
        }

#if WINDOWS_PHONE
        
        private void UnloadMixedCollection()
        {
            if (_filteredData.ResultData != null)
            {
                for (int ix = _mixedCollection.Count - 1; ix >= 0; ix--)
                {
                    if (!App.IsCoordinateVisibleForUser(_mixedCollection[ix].Position))
                        _mixedCollection.RemoveAt(ix);
                }
            }
        }

        private void UpdateMixedCollection()
        {
            if (_filteredData.ResultData != null)
            {
                _unloadCollectionTimer.Stop();

                for (int ix = _mixedCollection.Count - 1; ix >= 0; ix--)
                {
                    if (!_filteredData.ResultData.Contains(_mixedCollection[ix]))
                        _mixedCollection.RemoveAt(ix);
                }

                //add items that became visible for current moment
                var previousCount = _mixedCollection.Count();
                foreach (var sourceItem in _filteredData.ResultData)
                {
                    if (App.IsCoordinateVisibleForUser(sourceItem.Position) && 
                        !_mixedCollection.Contains(sourceItem))
                        _mixedCollection.Add(sourceItem);
                }

                Debug.WriteLine("UpdateMixedCollection previous = " + previousCount.ToString() + " current = " + _mixedCollection.Count().ToString());

                //if there ware any chages in collection schedule unloading unused items
                _unloadCollectionTimer.Start();
            }
        }

#endif

        public List<ToiletPushPinViewModel> MixedDataSource
        {
            get
            {
                if (_dataSource == null)
                    LoadDataAsync(_selectedDataSource);

#if DEBUG
                if (_filteredData.ResultData != null && _filteredData.ResultData.Count != 0)
                    Debug.WriteLine("MixedDataSource collection posted to UI rendering");
#endif
                return (_filteredData.ResultData);
            }
        }

        private Task<List<ToiletPushPinViewModel>> LoadDataInternalAsync(DtMosDataProvider.StorageType type)
        {
            return (_provider.GetDataSource(type)).ContinueWith((result) =>
            {
                var data = result.Result;
                var source = data.Select(dtMosToilet => new ToiletPushPinViewModel(dtMosToilet, type)).ToList();
                
                return (source);
            });
        }

        private Task LoadDataAsync(DtMosDataProvider.StorageType type)
        {
#if DEBUG
            Debug.WriteLine("LoadDataAsync started");
            var watch = new Stopwatch();
            watch.Start();
#endif

            IsOperationInProgress = true;
            var context = TaskScheduler.FromCurrentSynchronizationContext();

            if ((type & DtMosDataProvider.StorageType.OfficialDataMoscow) == type)
            {
                return (LoadDataInternalAsync(DtMosDataProvider.StorageType.OfficialDataMoscow)).ContinueWith((source) =>
                    {
                        IsOperationInProgress = false;

                        if (_dataSource == null)
                        {
                            _dataSource = new Dictionary<DtMosDataProvider.StorageType, List<ToiletPushPinViewModel>>
                                {
                                    {DtMosDataProvider.StorageType.OfficialDataMoscow, source.Result}
                                };
                        }
                        else
                        {
                            if (_dataSource.ContainsKey(DtMosDataProvider.StorageType.OfficialDataMoscow))
                                _dataSource[DtMosDataProvider.StorageType.OfficialDataMoscow] = source.Result;
                            else
                                _dataSource.Add(DtMosDataProvider.StorageType.OfficialDataMoscow, source.Result);
                        }
                        
                        _filteredData.SourceData = _dataSource[DtMosDataProvider.StorageType.OfficialDataMoscow];
                        RaisePropertyChanged("OfficialDataSource");
                    }, context);
            }
            else if ((type & DtMosDataProvider.StorageType.OpenData) == type)
            {
                return (LoadDataInternalAsync(DtMosDataProvider.StorageType.OpenData)).ContinueWith((source) =>
                    {
                        IsOperationInProgress = false;

                        if (_dataSource == null)
                        {
                            _dataSource = new Dictionary<DtMosDataProvider.StorageType, List<ToiletPushPinViewModel>>
                                {
                                    {DtMosDataProvider.StorageType.OpenData, source.Result}
                                };
                        }
                        else
                        {
                            if (_dataSource.ContainsKey(DtMosDataProvider.StorageType.OfficialDataMoscow))
                                _dataSource[DtMosDataProvider.StorageType.OpenData] = source.Result;
                            else
                                _dataSource.Add(DtMosDataProvider.StorageType.OpenData, source.Result);
                        }

                        _filteredData.SourceData = _dataSource[DtMosDataProvider.StorageType.OpenData];
                        RaisePropertyChanged("PeopeChoiceDataSource");
                    }, context); 
            }
            else
            {
                var token = new Task<Dictionary<DtMosDataProvider.StorageType, List<ToiletPushPinViewModel>>>(() =>
                    {
                        var tokenList = new Task<List<ToiletPushPinViewModel>>[]
                            {
                                LoadDataInternalAsync(DtMosDataProvider.StorageType.OfficialDataMoscow),
                                LoadDataInternalAsync(DtMosDataProvider.StorageType.OpenData)
                            };

                        Task.WaitAll(tokenList);

                        var result = new Dictionary<DtMosDataProvider.StorageType, List<ToiletPushPinViewModel>>();
                        var mix = new List<ToiletPushPinViewModel>();

                        //check probable exceptions for each async task
                        try
                        {
                            result.Add(DtMosDataProvider.StorageType.OfficialDataMoscow, tokenList[0].Result);
                            mix.AddRange(tokenList[0].Result);
                        }
                        catch (Exception ex)
                        {
                        }

                        try
                        {
                            result.Add(DtMosDataProvider.StorageType.OpenData, tokenList[1].Result);
                            mix.AddRange(tokenList[1].Result);
                        }
                        catch (Exception ex)
                        {
                        }

                        result.Add(DtMosDataProvider.StorageType.OfficialDataMoscow |
                            DtMosDataProvider.StorageType.OpenData, mix);

                        return (result);
                    });
                token.Start(TaskScheduler.Default);

                return (token).ContinueWith((source) =>
                    {
#if DEBUG
                        watch.Stop();
                        Debug.WriteLine("LoadDataAsync completed; time elapsed(ms) = " + watch.ElapsedMilliseconds.ToString());

                        watch.Reset();
                        watch.Start();
#endif

                        IsOperationInProgress = false;

                        _dataSource = source.Result;

                        _filteredData.SourceData = _dataSource[DtMosDataProvider.StorageType.OfficialDataMoscow |
                            DtMosDataProvider.StorageType.OpenData];
#if DEBUG
                        watch.Stop();
                        Debug.WriteLine("MixedDataSource changed; time elapsed(ms) = " + watch.ElapsedMilliseconds.ToString());
#endif
                    }, context); 
            }
        }

        public ICommand AjustCollectionToVisibleField
        {
            get 
            { 
                return new RelayCommand<bool>((p) =>
                    {
                        RaisePropertyChanged("MixedDataSource");
#if WINDOWS_PHONE
                        _mapRequestTimer.Stop();
                        _mapRequestTimer.Start();
#endif
                    });
            }
        }

        public bool EnableTimeFilter 
        { 
            get { return (_filteredData.HasFilter(FilterType.TimeFilter)); } 
            
            set
            {
                if (_filteredData.HasFilter(FilterType.TimeFilter))
                {
                    _filteredData.RemoveFilter(FilterType.TimeFilter);
                    App.Settings.SetValue("MainViewModel.TimeFilter", false);
                }
                else
                {
                    _filteredData.AddFilter(FilterType.TimeFilter);
                    App.Settings.SetValue("MainViewModel.TimeFilter", true);
                }
            } 
        }

        public bool AccessibilityFilter
        {
            get { return (_filteredData.HasFilter(FilterType.AccessibilityFilter)); }

            set
            {
                if (_filteredData.HasFilter(FilterType.AccessibilityFilter))
                {
                    _filteredData.RemoveFilter(FilterType.AccessibilityFilter);
                    App.Settings.SetValue("MainViewModel.AccessibilityFilter", false);
                }
                else
                {
                    _filteredData.AddFilter(FilterType.AccessibilityFilter);
                    App.Settings.SetValue("MainViewModel.AccessibilityFilter", true);
                }
            }
        }

        /*public int DistanceFilter 
        { 
            get 
            {
                try
                {
                    return (int) (_filteredData.QueryFilterArgs(FilterType.DistanceFilter));
                }
                catch (KeyNotFoundException ex)
                {
                    return (-1);
                }
            }
 
            set
            {
                if (_filteredData.HasFilter(FilterType.DistanceFilter))
                    _filteredData.RemoveFilter(FilterType.DistanceFilter);
                else
                    _filteredData.AddFilter(FilterType.DistanceFilter, value);
            }
        }*/

#if WINDOWS_STORE
        [Windows.UI.Xaml.Data.Bindable]
#endif
        public class SourceItem
        {
            internal DtMosDataProvider.StorageType Type;

            public string Name { get; set; }

            public SolidColorBrush BackgroundColor1
            { 
                get
                {
                    switch (Type)
                    {
#if WINDOWS_STORE
                        case DtMosDataProvider.StorageType.OfficialDataMoscow:
                            return (new SolidColorBrush(Windows.UI.ColorHelper.FromArgb(255, 69, 69, 229)));
                        case DtMosDataProvider.StorageType.OpenData:
                            return (new SolidColorBrush(Windows.UI.ColorHelper.FromArgb(255, 112, 179, 18)));
                        case DtMosDataProvider.StorageType.OpenData | DtMosDataProvider.StorageType.OfficialDataMoscow:
                            return (new SolidColorBrush(Windows.UI.ColorHelper.FromArgb(255, 112, 179, 18)));
#else
                        case DtMosDataProvider.StorageType.OfficialDataMoscow:
                            return (new SolidColorBrush(Color.FromArgb(255, 69, 69, 229)));
                        case DtMosDataProvider.StorageType.OpenData:
                            return (new SolidColorBrush(Color.FromArgb(255, 112, 179, 18)));
                        case DtMosDataProvider.StorageType.OpenData | DtMosDataProvider.StorageType.OfficialDataMoscow:
                            return (new SolidColorBrush(Color.FromArgb(255, 112, 179, 18)));
#endif

                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                } 
            }

            public SolidColorBrush BackgroundColor2
            {
                get
                {
                    switch (Type)
                    {
#if WINDOWS_STORE
                        case DtMosDataProvider.StorageType.OfficialDataMoscow:
                            return (new SolidColorBrush(Windows.UI.ColorHelper.FromArgb(255, 69, 69, 229)));
                        case DtMosDataProvider.StorageType.OpenData:
                            return (new SolidColorBrush(Windows.UI.ColorHelper.FromArgb(255, 112, 179, 18)));
                        case DtMosDataProvider.StorageType.OpenData | DtMosDataProvider.StorageType.OfficialDataMoscow:
                            return (new SolidColorBrush(Windows.UI.ColorHelper.FromArgb(255, 69, 69, 229)));
#else
                        case DtMosDataProvider.StorageType.OfficialDataMoscow:
                            return (new SolidColorBrush(Color.FromArgb(255, 69, 69, 229)));
                        case DtMosDataProvider.StorageType.OpenData:
                            return (new SolidColorBrush(Color.FromArgb(255, 112, 179, 18)));
                        case DtMosDataProvider.StorageType.OpenData | DtMosDataProvider.StorageType.OfficialDataMoscow:
                            return (new SolidColorBrush(Color.FromArgb(255, 69, 69, 229)));
#endif
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }

        private List<SourceItem> _sourceTypeList; 

        public List<SourceItem> SourceTypeList
        {
            get
            {
                if (_sourceTypeList == null)
                {
                    _sourceTypeList = new List<SourceItem>
                        {
                            new SourceItem()
                                {
                                    Type = DtMosDataProvider.StorageType.OfficialDataMoscow,
                                    Name = "������ � data.mos.ru"
                                },
                            new SourceItem()
                                {
                                    Type = DtMosDataProvider.StorageType.OpenData,
                                    Name = "�������� ������"
                                },
                            new SourceItem()
                                {
                                    Type =
                                        DtMosDataProvider.StorageType.OfficialDataMoscow |
                                        DtMosDataProvider.StorageType.OpenData,
                                    Name = "���"
                                }
                        };
                }

                return (_sourceTypeList);
            }
        }

        public SourceItem SourceTypeItem
        {
            get { return (SourceTypeList.FirstOrDefault(t => t.Type == _selectedDataSource));}
            set
            {
                _selectedDataSource = value.Type;
                App.Settings.SetValue("MainViewModel.SelectedDataSource", (int)value.Type);

                if (_dataSource.ContainsKey(_selectedDataSource))
                    _filteredData.SourceData = _dataSource[_selectedDataSource];
                else
                    LoadDataAsync(_selectedDataSource);
                RaisePropertyChanged("SourceTypeItem");
            }
        }

        private bool _isOperationInProgress = false;

        public bool IsOperationInProgress
        {
            get { return _isOperationInProgress; }
            private set 
            {
                _isOperationInProgress = value;
                RaisePropertyChanged("IsOperationInProgress");
            }
        }

        /// <summary>
        /// Default map center is center of Moscow
        /// </summary>
        private GeoCoordinate _mapCenter = new GeoCoordinate(55.751896, 37.623164);

        public GeoCoordinate MapCenter
        {
            get { return (_mapCenter); }
            set 
            {
                _mapCenter = value;
                RaisePropertyChanged("MapCenter");
            }
        }

        private bool _dataSourceReady = false;

        public bool DataSourceReady
        {
            get { return (_dataSourceReady); }
            set
            {
                var previous = _dataSourceReady;
                _dataSourceReady = true;

                if (previous != _dataSourceReady)
                    RaisePropertyChanged("DataSourceReady");
            }
        }
        
        public string GeoLocationStatus
        {
            get
            {
                if (_positionPushPin != null)
                    return (_positionPushPin.GeoLocationStatus);
                else
                    return ("");
            }
        }

        private ICommand _aboutPageNavigateCommand;

        public ICommand AboutPageNavigateCommand
        {
            get
            {
                return _aboutPageNavigateCommand ?? (_aboutPageNavigateCommand = 
                    new RelayCommand(() => ((App) Application.Current).RootFrame.NavigateToView(
                        new Uri("/View/Pages/AboutPage.xaml", UriKind.Relative), null)));
            }
        }

        private ICommand _showUserLocationCommand;

        public ICommand ShowUserLocationCommand
        {
            get
            {
                return _showUserLocationCommand ?? (_showUserLocationCommand = new RelayCommand(() =>
                    {
                        if (_positionPushPin != null && _positionPushPin.Position != null)
                            MapCenter = _positionPushPin.Position;
                    }));
            }
        }

        private ICommand _addNewToiletCommand;

        public ICommand AddNewToiletCommand
        {
            get
            {
                if (_addNewToiletCommand == null)
                {
                    _addNewToiletCommand =
                        new RelayCommand(() =>
                            {
                                System.Diagnostics.Debug.Assert(false);
                                ((App) Application.Current).RootFrame.NavigateToView(
                                    new Uri("/View/Pages/AddNewToiletPage.xaml", UriKind.Relative), null);
                            });
                }
                return _showUserLocationCommand;
            }
        }

        private ICommand _showSettingsCommand;

        public ICommand ShowSettingsCommand
        {
            get
            {
                return _showSettingsCommand ?? (_showSettingsCommand = 
                    new RelayCommand(() => ((App) Application.Current).RootFrame.NavigateToView(
                        new Uri("/View/Pages/SettingsPage.xaml", UriKind.Relative), null)));
            }
        }
    }
}