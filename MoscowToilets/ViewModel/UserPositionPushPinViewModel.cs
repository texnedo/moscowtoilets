﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MoscowToilets.Model.Geo;
using Yandex.Positioning;

#if WINDOWS_STORE
using Windows.UI.Xaml;
#elif WINDOWS_PHONE && WP7
using MoscowToiletsWP7;
using System.Windows;
#elif WINDOWS_PHONE && WP8
using MoscowToiletsWP8;
using System.Windows;
#endif

namespace MoscowToilets.ViewModel
{

    public class UserPositionPushPinViewModel : BasePushPinViewModel
    {
        private GeoLocationProvider _geoLocation;

        private Visibility _contentVisibility = Visibility.Visible;

        private Visibility _visibility = Visibility.Visible;

        public override Visibility ContentVisibility
        {
            get { return (Visibility); }
            set 
            { 
                _contentVisibility = value;
                RaisePropertyChanged("ContentVisibility");
            }
        }

        public string GeoLocationStatus
        {
            get
            {
                if (_geoLocation == null)
                    return ("Определение текущих координат отключено");

                switch (_geoLocation.LocationState.State)
                {
                    case GeoLocationProviderState.PositionUnavalible:
                        return ("Определение текущих координат недосупно");
                    case GeoLocationProviderState.PositionCached:
                        return ("Доступна предыдущая позиция");
                    case GeoLocationProviderState.LookingForPosition:
                        return ("Выполняется определение текущей позиции...");
                    case GeoLocationProviderState.PositionAvalible:
                        return ("Текущие координаты обновлены");
                    case GeoLocationProviderState.PositionDisabled:
                        return ("Определение текущих координат отключено");
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public override Visibility Visibility
        {
            get
            {
                if (_geoLocation == null)
                    return (Visibility.Collapsed);

                switch (_geoLocation.LocationState.State)
                {
                    case GeoLocationProviderState.PositionUnavalible:
                    case GeoLocationProviderState.PositionCached:
                    case GeoLocationProviderState.LookingForPosition:
                        {
                            if (_geoLocation.LocationState.Position != null && 
                                App.Locator.Settings.UseCachedPositionWhenGpsUnavailable)
                                return (Visibility.Visible);

                            return (Visibility.Collapsed);
                        }
                    case GeoLocationProviderState.PositionAvalible:
                        return (Visibility.Visible);
                    case GeoLocationProviderState.PositionDisabled:
                        return (Visibility.Collapsed);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            set
            {
                _visibility = value;
                RaisePropertyChanged("Visibility");
            }
        }

        public override GeoCoordinate Position
        {
            get
            {
                if (_geoLocation == null)
                    return (null);

                return (_geoLocation.LocationState.Position);             
            }
        }

        public bool IsLookingForPosition
        {
            get 
            {
                if (_geoLocation == null)
                    return (false);

                switch (_geoLocation.LocationState.State)
                {
                    case GeoLocationProviderState.PositionAvalible:
                    case GeoLocationProviderState.PositionDisabled:
                        return (false);
                    case GeoLocationProviderState.LookingForPosition:
                    case GeoLocationProviderState.PositionCached:
                    case GeoLocationProviderState.PositionUnavalible:
                        return (true);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public UserPositionPushPinViewModel()
        {
            if (App.Locator.Settings.UseGpsLocationData)
            {
                _geoLocation = new GeoLocationProvider();
                _geoLocation.PropertyChanged += (obj, args) =>
                {
                    if (args.PropertyName == "LocationState")
                    {
                        RaisePropertyChanged("IsLookingForPosition");
                        RaisePropertyChanged("Visibility");
                        RaisePropertyChanged("ContentVisibility");
                        RaisePropertyChanged("Position");
                        RaisePropertyChanged("GeoLocationStatus");
                    }
                };   
            }

            App.Locator.Settings.PropertyChanged += (sender, arg) =>
                {
                    if (App.Locator.Settings.UseGpsLocationData)
                    {
                        _geoLocation = new GeoLocationProvider();
                        _geoLocation.PropertyChanged += (obj, args) =>
                            {
                                if (args.PropertyName == "LocationState")
                                {
                                    RaisePropertyChanged("IsLookingForPosition");
                                    RaisePropertyChanged("Visibility");
                                    RaisePropertyChanged("ContentVisibility");
                                    RaisePropertyChanged("Position");
                                    RaisePropertyChanged("GeoLocationStatus");
                                }
                            };

                    }
                    else
                        _geoLocation = null;

                    RaisePropertyChanged("IsLookingForPosition");
                    RaisePropertyChanged("Visibility");
                    RaisePropertyChanged("ContentVisibility");
                    RaisePropertyChanged("Position");
                    RaisePropertyChanged("GeoLocationStatus");
                };
        }
    }
}
