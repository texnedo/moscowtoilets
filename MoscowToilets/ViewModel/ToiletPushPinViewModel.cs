﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Phone.Net.NetworkInformation;
using Yandex.Maps;
using MoscowToilets.Model.DataMos;
using Yandex.Positioning;

#if WINDOWS_STORE
using Windows.UI.Xaml.Media.Imaging;
using System.Windows.Input;
#elif WINDOWS_PHONE
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Input;
#if WP7
using MoscowToiletsWP7;
#endif
#if WP8
using MoscowToiletsWP8;
#endif
#endif

namespace MoscowToilets.ViewModel
{
    public class ToiletPushPinViewModel : BasePushPinViewModel
    {
        private bool _selected = false;

        private readonly DtMosToilet _itemModel;

        public override GeoCoordinate Position
        {
            get { return (new GeoCoordinate(_itemModel.WGS84Coordinate.X, _itemModel.WGS84Coordinate.Y)); }

            set { }
        }

        private Visibility _visibility;

        public override Visibility Visibility
        {
            get { return (_visibility); }
            set { _visibility = value; }
        }

        public string Address { get { return (_itemModel.Address); } }

        public string Description
        {
            get
            {
                if (!string.IsNullOrEmpty(_itemModel.Details))
                    return (_itemModel.Details);
                else
                    return ("---");
            }
        }

        public string Timetable
        {
            get
            {
                if (!string.IsNullOrEmpty(_itemModel.WorkTimeTable))
                    return (_itemModel.WorkTimeTable);
                else
                    return ("---");
            }
        }

        public bool IsOpen { get { return (_itemModel.IsOpen); } }

        public bool IsOpenNow
        {
            get
            {
                if (_itemModel.OpenFrom == null || _itemModel.OpenTo == null)   
                    return (false);

                var current = new TimeSpan(0, DateTime.Now.Hour, DateTime.Now.Minute, 0);
                if (current > _itemModel.OpenFrom && current < _itemModel.OpenTo)
                    return (true);
                else
                    return (false);
            }
        }

        public bool Availability { get { return (_itemModel.Availability); } }

        public string AvailabilityText
        {
            get
            {
                if (Availability)
                    return ("Приспособлен");
                else
                    return ("Не приспособлен");
            }
        }

        private readonly DtMosDataProvider.StorageType _type;

        public BitmapImage PushPinImage
        {
            get
            {
                switch (Type)
                {
#if WINDOWS_STORE
                    case DtMosDataProvider.StorageType.OfficialDataMoscow:
                        if (!Selected)
                            return (new BitmapImage(new Uri("ms-appx:///Resources/Images/pin_blue.png")));
                        return (new BitmapImage(new Uri("ms-appx:///Resources/Images/pin_blue_selected.png")));
                    case DtMosDataProvider.StorageType.OpenData:
                        if (!Selected)
                            return (new BitmapImage(new Uri("ms-appx:///Resources/Images/pin_green.png")));
                        return (new BitmapImage(new Uri("ms-appx:///Resources/Images/pin_green_selected.png")));
#else
                    case DtMosDataProvider.StorageType.OfficialDataMoscow:
                        if (!_selected)
                            return (new BitmapImage(new Uri("/Resources/Images/pin_blue.png", UriKind.Relative)));
                        return (new BitmapImage(new Uri("/Resources/Images/pin_blue_selected.png", UriKind.Relative)));
                    case DtMosDataProvider.StorageType.OpenData:
                        if (!_selected)
                            return (new BitmapImage(new Uri("/Resources/Images/pin_green.png", UriKind.Relative)));
                        return (new BitmapImage(new Uri("/Resources/Images/pin_green_selected.png", UriKind.Relative)));
#endif
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public BitmapImage BigPushPinImage
        {
            get
            {
                switch (Type)
                {
#if WINDOWS_STORE
                    case DtMosDataProvider.StorageType.OfficialDataMoscow:
                        return (new BitmapImage(new Uri("ms-appx:///Resources/Images/pin_blue_selected.png")));
                    case DtMosDataProvider.StorageType.OpenData:
                        return (new BitmapImage(new Uri("ms-appx:///Resources/Images/pin_green_selected.png")));
#else
                    case DtMosDataProvider.StorageType.OfficialDataMoscow:
                        return (new BitmapImage(new Uri("/Resources/Images/pin_blue_selected.png", UriKind.Relative)));
                    case DtMosDataProvider.StorageType.OpenData:
                        return (new BitmapImage(new Uri("/Resources/Images/pin_green_selected.png", UriKind.Relative)));
#endif
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double IsOpenOpacity 
        {
            get
            {
                if (IsOpen)
                    return (1);
                else
                    return (0.4);
            }
        }

        public Uri NearestLocationPhotoSmall
        {
            get
            {
#if WINDOWS_PHONE
                if (string.IsNullOrEmpty(_itemModel.NearestLocationPhotoSmall) ||
                    !DeviceNetworkInformation.IsNetworkAvailable)
                    return (new Uri("/ApplicationIcon.png", UriKind.Relative));
#elif WINDOWS_STORE
#error
#endif

                return (new Uri(_itemModel.NearestLocationPhotoSmall));
            }
        }

        public Uri NearestLocationPhotoBig
        {
            get
            {
                if (string.IsNullOrEmpty(_itemModel.NearestLocationPhotoBig))
                    return (null);

                return (new Uri(_itemModel.NearestLocationPhotoBig));
            }
        }

        public bool Selected
        {
            get { return _selected; }
            private set
            {
                _selected = value;
                RaisePropertyChanged("Selected");
                RaisePropertyChanged("PushPinImage");
            }
        }

        public ICommand ChangeSelectionCommand
        {
            get 
            { 
                return new RelayCommand<bool>((p) =>
                    {
                        Selected = !Selected;
                    });
            }
        }

        public ICommand ViewBigPhotoCommand
        {
            get
            {
                return new RelayCommand<bool>((p) =>
                    {
                        if (DeviceNetworkInformation.IsNetworkAvailable)
                                ((App) Application.Current).RootFrame.NavigateToView(
                                    new Uri("/View/Pages/BigImageViewerPage.xaml", UriKind.Relative), this);
                    });
            }
        }

        public ICommand ShowFullDescriptionCommand
        {
            get
            {
                return new RelayCommand<bool>((p) => ((App)Application.Current).RootFrame.NavigateToView(
                    new Uri("/View/Pages/ToitetDescriptionPage.xaml", UriKind.Relative), this));
            }
        }

        internal DtMosDataProvider.StorageType Type { get { return _type; } }

        internal string Id
        {
            get
            {
                switch (_type)
                {
                    case DtMosDataProvider.StorageType.OfficialDataMoscow:
                        return ("OfficialDataMoscow" + _itemModel.Code);
                    case DtMosDataProvider.StorageType.OpenData:
                        return ("OpenData" + _itemModel.Code);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public List<ToiletPushPinViewModel> PositionDataSource
        {
            get
            {
                return (new List<ToiletPushPinViewModel> {this});
            }
        }

        internal ToiletPushPinViewModel(DtMosToilet item, DtMosDataProvider.StorageType type)
        {
            _itemModel = item;
            _type = type;
        }
    }
}
