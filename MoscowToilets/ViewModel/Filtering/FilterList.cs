﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace MoscowToilets.ViewModel.Filtering
{
    internal class FilterList : ObservableObject
    {
        private readonly Dictionary<FilterType, BaseFilter> _filters = new Dictionary<FilterType, BaseFilter>();

        private List<ToiletPushPinViewModel> _sourceData;

        private List<ToiletPushPinViewModel> _cachedFilteredData;

        internal FilterList()
        {
        }

        internal List<ToiletPushPinViewModel> ResultData
        {
            get
            {
                if (_filters.Count == 0 || _sourceData == null)
                    return (_sourceData);

                return (_cachedFilteredData);
            }
        }

        private bool _isOperationInProgress = false;

        public bool IsOperationInProgress
        {
            get { return _isOperationInProgress; }
            private set
            {
                _isOperationInProgress = value;
                RaisePropertyChanged("IsOperationInProgress");
            }
        }

        private Task<List<ToiletPushPinViewModel>> FilterDataAsync()
        {
            var filters = _filters.Values.ToList();
            var data = _sourceData == null ? null : _sourceData.ToList();

            var token = new Task<List<ToiletPushPinViewModel>>(() =>
                {
                    Debug.WriteLine("FilterDataAsync in Filterlist started");

                    List<ToiletPushPinViewModel> result = null;
                    if (filters.Count == 0 || data == null)
                        result = data;
                    else
                        result = filters.Aggregate(data, (current, baseFilter) => baseFilter.ApplyToCollection(current));

                    Debug.WriteLine("FilterDataAsync in Filterlist completed");

                    return (result);
                });
            token.Start();

            return (token);
        }

        public List<ToiletPushPinViewModel> SourceData
        {
            get { return _sourceData; }
            set
            {
                _sourceData = value;
                UpdateDataAsync();
            }
        }

        internal void RefreshData()
        {
            UpdateDataAsync();
        }

        private Task UpdateDataAsync()
        {
#if DEBUG
            Debug.WriteLine("UpdateDataAsync in Filterlist started");
            var watch = new Stopwatch();
            watch.Start();
#endif

            IsOperationInProgress = true;
            return(FilterDataAsync()).ContinueWith((data) =>
                {
#if DEBUG
                    watch.Stop();
                    Debug.WriteLine("ResultData changed in Filterlist; time elapsed(ms) = " + watch.ElapsedMilliseconds.ToString());
#endif

                    IsOperationInProgress = false;

                    _cachedFilteredData = data.Result;
                    RaisePropertyChanged("ResultData");
                }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        internal void AddFilter(FilterType type, object args = null)
        {
            _filters.Add(type, BaseFilter.CreateFilter(type, args));
            UpdateDataAsync();
        }

        internal void RemoveFilter(FilterType type)
        {
            _filters.Remove(type);
            UpdateDataAsync();
        }

        internal bool HasFilter(FilterType type)
        {
            return (_filters.ContainsKey(type));
        }

        internal object QueryFilterArgs(FilterType type)
        {
            return (_filters[type].GetFilterArgs());
        }
    }
}
