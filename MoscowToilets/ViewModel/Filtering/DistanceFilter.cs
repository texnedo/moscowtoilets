﻿using System.Collections.Generic;
using System.Linq;

namespace MoscowToilets.ViewModel.Filtering
{
    internal class DistanceFilter: BaseFilter
    {
        private readonly int _distance = -1;

        internal DistanceFilter(int distance)
        {
            _distance = distance;
        }

        protected override List<ToiletPushPinViewModel> FilterCollection(List<ToiletPushPinViewModel> data)
        {
            return (data.Where(item => item.Availability == true).ToList());
        }

        internal override object GetFilterArgs()
        {
            return (_distance);
        }
    }
}
