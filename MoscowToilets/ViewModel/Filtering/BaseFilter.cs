﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace MoscowToilets.ViewModel.Filtering
{
    internal enum FilterType
    {
        AccessibilityFilter = 0,
        DistanceFilter = 1,
        TimeFilter = 2,
        GeoVisibilityFilter = 3
    }

    internal abstract class BaseFilter
    {
        internal List<ToiletPushPinViewModel> ApplyToCollection(List<ToiletPushPinViewModel> data)
        {
            if (data == null)
            {
                Debug.Assert(false);
                return (null);
            }

            return (FilterCollection(data));
        }

        internal virtual object GetFilterArgs()
        {
            return (null);
        }

        protected abstract List<ToiletPushPinViewModel> FilterCollection(List<ToiletPushPinViewModel> data);
    
        internal static BaseFilter CreateFilter(FilterType type, object args = null)
        {
            switch (type)
            {
                case FilterType.AccessibilityFilter:
                    return (new AccessibilityFilter());
                case FilterType.DistanceFilter:
                    return (new DistanceFilter(args is int ? (int) args : -1));
                case FilterType.TimeFilter:
                    return (new TimeFilter());
                case FilterType.GeoVisibilityFilter:
                    return (new GeoVisibilityFilter());
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }
    }
}
