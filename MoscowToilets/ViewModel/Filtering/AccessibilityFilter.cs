﻿using System.Collections.Generic;
using System.Linq;

namespace MoscowToilets.ViewModel.Filtering
{
    internal class AccessibilityFilter : BaseFilter
    {
        protected override List<ToiletPushPinViewModel> FilterCollection(List<ToiletPushPinViewModel> data)
        {
            return (data.Where(item => item.Availability == true).ToList());
        }
    }
}