﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MoscowToilets.ViewModel.Filtering 
{
    class TimeFilter : BaseFilter
    {
        protected override List<ToiletPushPinViewModel> FilterCollection(List<ToiletPushPinViewModel> data)
        {
           return (data.Where(item => item.IsOpenNow == true).ToList());
        }
    }
}
