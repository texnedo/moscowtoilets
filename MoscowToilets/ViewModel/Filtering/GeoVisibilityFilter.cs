﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if WP7
using MoscowToiletsWP7;
#elif WP8
using MoscowToiletsWP8;
#endif

namespace MoscowToilets.ViewModel.Filtering
{
    class GeoVisibilityFilter : BaseFilter
    {
        protected override List<ToiletPushPinViewModel> FilterCollection(List<ToiletPushPinViewModel> data)
        {
#if WINDOWS_PHONE
            return (data.Where(sourceItem => App.IsCoordinateVisibleForUser(sourceItem.Position)).ToList());
#elif WINDOWS_STORE
            return (data);
#endif
        }
    }
}
