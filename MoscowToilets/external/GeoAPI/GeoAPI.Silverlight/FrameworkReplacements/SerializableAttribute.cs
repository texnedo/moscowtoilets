#if SILVERLIGHT ||  NETFX_CORE
namespace System
{
    public class SerializableAttribute : Attribute
    {
    }
    public class NonSerializedAttribute : Attribute
    {
    }

}
#endif