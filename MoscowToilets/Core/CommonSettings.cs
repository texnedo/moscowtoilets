﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

#if WINDOWS_STORE
using Windows.Foundation.Collections;
using Windows.Storage;
#elif WP7
using System.IO.IsolatedStorage;
using System.Windows;
#elif WP8
using System.IO.IsolatedStorage;
using System.Windows;
#endif

namespace MoscowToilets.Core
{
#if WINDOWS_STORE
    internal class SettingsEventArgs
#elif WINDOWS_PHONE
    internal class SettingsEventArgs : EventArgs
#endif
    {
        internal string Name { get; set; }

        internal object OldValue { get; set; }

        internal object NewValue { get; set; }
    }

    internal class CommonSettings 
    {
        private const string CONTAINER_NAME = "CommonSettings";

        private readonly object _syncObject = new object();

#if WINDOWS_STORE
        private readonly IPropertySet _container;
#elif WINDOWS_PHONE
        private readonly IDictionary<string, object> _container;
#endif

        public event EventHandler<SettingsEventArgs> SettingsChanged; 

        public CommonSettings()
        {
#if WINDOWS_STORE
            if (!ApplicationData.Current.LocalSettings.Containers.ContainsKey(CONTAINER_NAME))
            {
                _container = ApplicationData.Current.LocalSettings.CreateContainer(CONTAINER_NAME,
                                                                                   ApplicationDataCreateDisposition
                                                                                       .Always).Values;
            }
            else
                _container = ApplicationData.Current.LocalSettings.Containers[CONTAINER_NAME].Values;
#else
            _container = IsolatedStorageSettings.ApplicationSettings;
#endif
        }

        public string GetString(string name)
        {
            return ((string) GetValue(name));
        }

        public object GetValue(string name)
        {
            if (_container == null)
            {
                System.Diagnostics.Debug.Assert(false);
                return (null);
            }

            lock (_syncObject)
            {
                return (_container[name]);
            }
        }

        public void SetValue(string name, object value)
        {
            if (_container == null)
            {
                System.Diagnostics.Debug.Assert(false);
                return;
            }

            lock (_syncObject)
            {
                var data = new SettingsEventArgs
                {
                    Name = name,
                    NewValue = value,
                    OldValue = HasValue(name) ? GetValue(name) : null
                };

                if (SettingsChanged != null)
                    SettingsChanged(this, data);

                _container[name] = value;   
            }
        }

        public bool HasValue(string name)
        {
            lock (_syncObject)
            {
                return (_container.ContainsKey(name));
            }
        }

#if WINDOWS_PHONE
        public void Save()
        {
            lock (_syncObject)
            {
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }
#endif
    }
}
