﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if WINDOWS_PHONE
using System.Windows;
using System.Windows.Controls;
using GalaSoft.MvvmLight;
using Microsoft.Phone.Controls;
#elif WINDOWS_STORE
#endif

namespace MoscowToilets.Core
{
#if WINDOWS_PHONE
    public class ViewNagationFrame : PhoneApplicationFrame
#elif WINDOWS_STORE
    class ViewNagationFrame
#endif
    {
        private readonly Dictionary<int, WeakReference> _viewModelDictionary = new Dictionary<int, WeakReference>(); 
        
        public ViewNagationFrame()
        {
            Navigating += (obj, args) =>
                {
                    
                };

            Navigated += (obj, args) =>
                {
                    var frame = Application.Current.RootVisual as Frame;
                    if (frame != null && frame.Content != null && frame.Content is PhoneApplicationPage)
                    {
                        var query = args.Uri.OriginalString.Split(new char[] { '&' });
                        if (!query.Any())
                            return;

                        try
                        {
                            foreach (var s in query)
                            {
                                if (s.Contains("ViewModelId="))
                                {
                                    var tokens = s.Split(new char[] { '=' });
                                    var id = Convert.ToInt32(tokens[1]);
                                    if (tokens.Count() == 2 && _viewModelDictionary.ContainsKey(id))
                                    {
                                        var weakData = _viewModelDictionary[id];
                                        if (weakData.IsAlive)
                                            (frame.Content as PhoneApplicationPage).DataContext = weakData.Target;
                                            
                                        break;
                                    }
                                }
                            }
                        }
                        catch (FormatException ex)
                        {

                        }
                    }
                };
        }

       public bool NavigateToView(Uri pathToView, ViewModelBase viewModel)
       {
           if (pathToView == null)
               throw (new ArgumentNullException());

           if (viewModel != null)
           {
               var id = viewModel.GetHashCode();
               if (!_viewModelDictionary.ContainsKey(id))
                   _viewModelDictionary.Add(id, new WeakReference(viewModel));

               Uri path = null;
               if (!pathToView.OriginalString.Contains("?"))
                   path = new Uri(pathToView.OriginalString + "?ViewModelId=" + id, UriKind.Relative);
               else
                   path = new Uri(pathToView.OriginalString + "&ViewModelId=" + id, UriKind.Relative);

               return (Navigate(path));
           }
           else
               return (Navigate(pathToView));
       }
    }
}
