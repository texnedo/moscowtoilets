﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;
using GeoAPI.Geometries;
#if WINDOWS_STORE
using Windows.Data.Xml.Dom;
#else
using System.Xml;
#endif

namespace MoscowToilets.Model.DataMos
{
    [XmlRoot("data")]
    public class DtObjectsCollection
    {
        [XmlArray("items")]
        public List<DtMosToilet> Data { get; set; }
    }

    public class DtMosToilet
    {
        private const string GOOGLE_STREET_VIEW_API_URL 
            = "http://maps.googleapis.com/maps/api/streetview?size={0}x{1}&location={2},{3}&sensor=false&key=AIzaSyDR2fy0_hGdlQn_0i0uhrJXY3cjEzwQ5v0"; 

        [XmlAttribute("code")]
        [DtMosDataContract("0_ID")]
        public string Code { get; set; }

        [XmlAttribute("addr")]
        [DtMosDataContract("1_Адрес")]
        public string Address { get; set; }

        private string _WorkTimeTable;

        private bool _IsOpen = true;

        public bool IsOpen { get { return (_IsOpen); } }

        [XmlAttribute("time")]
        [DtMosDataContract("1_Режим работы")]
        public string WorkTimeTable
        {
            get { return (_WorkTimeTable); }
            set
            {
                _WorkTimeTable = value;
                if (!string.IsNullOrEmpty(_WorkTimeTable) && _WorkTimeTable.Contains("закрыт"))
                    _IsOpen = false;
                else if (!string.IsNullOrEmpty(_WorkTimeTable))
                {
                    var pattern = new Regex("(?<from1>[0-9]{1,2})[\\.-]+(?<to1>[0-9]{1,2})[^\\d]*(?<from2>[0-9]{1,2})[\\.-]+(?<to2>[0-9]{1,2})");
                    var match = pattern.Match(_WorkTimeTable);
                    
                    if (match.Success)
                    {
                        try
                        {
                            var from1 = Convert.ToInt32(match.Groups["from1"].ToString());
                            var to1 = Convert.ToInt32(match.Groups["to1"].ToString());
                            OpenFrom = new TimeSpan(from1, to1, 0);

                            var from2 = Convert.ToInt32(match.Groups["from2"].ToString());
                            var to2 = Convert.ToInt32(match.Groups["to2"].ToString());
                            OpenTo = new TimeSpan(from2, to2, 0);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
        }

        internal TimeSpan OpenFrom { get; private set; }

        internal TimeSpan OpenTo { get; private set; }

        private string _AvailabilityValue;

        private bool _Availability = true;

        [XmlAttribute("avail")]
        [DtMosDataContract("1_Доступность")]
        public string AvailabilityValue
        {
            get { return (_AvailabilityValue); }
            set
            {
                _AvailabilityValue = value;
                if (!string.IsNullOrEmpty(_AvailabilityValue) && _AvailabilityValue.Contains("Не приспособлен"))
                {   
                    _Availability = false;
                    return;
                }

                bool original = _Availability;
                if (!bool.TryParse(_AvailabilityValue, out _Availability))
                    _Availability = original;
            }
        }

        public bool Availability { get { return (_Availability); } }

        private readonly string _CsvSourceLine;

        public DtMosToilet()
        {

        }

        public DtMosToilet(string csvLine, Dictionary<int, PropertyInfo> parseRule)
        {
            _CsvSourceLine = csvLine.Replace("\"", "");

            var dataTokens = _CsvSourceLine.Split(new char[] { ';' });
            if (dataTokens.Length == 0 || parseRule == null)
                throw (new DtMosObjectParseException(csvLine));

            try
            {
                foreach (var item in parseRule)
                {
#if WINDOWS_STORE
                    item.Value.SetValue(this, dataTokens[item.Key]);
#else
                    item.Value.SetValue(this, dataTokens[item.Key], BindingFlags.Default, null, null, null);
#endif
                }
            }
            catch (Exception ex)
            {
                throw (new DtMosObjectParseException(csvLine + ex.ToString()));
            }
        }

        [XmlIgnore]
        public Coordinate WGS84Coordinate { get; private set; }

        [XmlIgnore]
        public Coordinate MercatorCoordinate { get; private set; }

        public string CsvSourceLine { get { return _CsvSourceLine; } }

        [XmlAttribute("descr")]
        [DtMosDataContract("Details")]
        public string Details { get; set; }

        [XmlAttribute("date")]
        [DtMosDataContract("Date")]
        public string Date { get; set; }

        [XmlAttribute("lat")]
        [DtMosDataContract("Lattitude")]
        public string XCoordinate
        {
            get
            {
                if (WGS84Coordinate == null)
                    return ("");

                return (WGS84Coordinate.X.ToString(CultureInfo.InvariantCulture));
            }
            set
            {
                if (WGS84Coordinate == null)
                    WGS84Coordinate = new Coordinate();

                try
                {
                    if (!string.IsNullOrEmpty(value))
                        WGS84Coordinate.X = Convert.ToDouble(value, CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                }
            }
        }

        [XmlAttribute("lon")]
        [DtMosDataContract("Longitude")]
        public string YCoordinate
        {
            get
            {
                if (WGS84Coordinate == null)
                    return ("");



                return (WGS84Coordinate.Y.ToString(CultureInfo.InvariantCulture));
            }
            set
            {
                if (WGS84Coordinate == null)
                    WGS84Coordinate = new Coordinate();

                try
                {
                    if (!string.IsNullOrEmpty(value))
                        WGS84Coordinate.Y = Convert.ToDouble(value, CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                }
            }
        }

        [XmlIgnore]
        public string NearestLocationPhotoSmall
        {
            get
            {
                return
                    (string.Format(GOOGLE_STREET_VIEW_API_URL, 100, 100, Uri.EscapeUriString(XCoordinate),
                                   Uri.EscapeUriString(YCoordinate)));
            }
        }

        [XmlIgnore]
        public string NearestLocationPhotoBig
        {
            get
            {
                return
                    (string.Format(GOOGLE_STREET_VIEW_API_URL, 200, 200, Uri.EscapeUriString(XCoordinate),
                                   Uri.EscapeUriString(YCoordinate)));
            }
        }
    }
}
