﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if WINDOWS_STORE
using System.Xml.Serialization;
using Windows.Data.Xml.Dom;
using Windows.Storage;
using System.IO.Compression;
#else
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using Ionic.Zlib;
using System.IO.IsolatedStorage;
#endif

namespace MoscowToilets.Model.DataMos
{
    internal class DtMosDataProvider
    {
        [Flags]
        internal enum StorageType
        {
            OfficialDataMoscow = 1 << 1,
            OpenData           = 1 << 2,
            UserData           = 1 << 3
        }

        private const string FILE_CACHE_FOLDER = "DataCache"; 

        internal DtMosDataProvider()
        {

        }

#if WINDOWS_STORE
        private async Task SaveToiletsDataFromXmlZipAsync(StorageFile file, List<DtMosToilet> data)
        {
            using (var stream = await file.OpenAsync(FileAccessMode.ReadWrite))
            {
                var compressedStream = stream.AsStreamForWrite();

                var collection = new DtObjectsCollection() { Data = data };
                var xmlStream = new MemoryStream();
                var serializer = new XmlSerializer(typeof(DtObjectsCollection));
                serializer.Serialize(xmlStream, collection);

                using (var compressionStream = new GZipStream(compressedStream, CompressionMode.Compress, true))
                {
                    await compressionStream.WriteAsync(xmlStream.ToArray(), 0, (int)xmlStream.Length);
                }

                await compressedStream.FlushAsync();
            }
        }
#else
        private Task SaveToiletsDataFromXmlZipAsync(string file, List<DtMosToilet> data)
        {
            var token = new Task(() =>
            {
                using (var stream = IsolatedStorageFile.GetUserStoreForApplication().OpenFile(file, FileMode.Open))
                {
                    var compressedStream = stream;

                    var collection = new DtObjectsCollection() { Data = data };
                    var xmlStream = new MemoryStream();
                    var serializer = new XmlSerializer(typeof(DtObjectsCollection));
                    serializer.Serialize(xmlStream, collection);

                    using (var compressionStream = new GZipStream(compressedStream, CompressionMode.Compress, true))
                    {
                        compressionStream.Write(xmlStream.ToArray(), 0, (int)xmlStream.Length);
                    }

                    compressedStream.Flush();
                }
            });
            token.Start();

            return (token);
        }
#endif

#if WINDOWS_STORE
        private async Task<List<DtMosToilet>> ReadToiletsDataFromXmlZipAsync(StorageFile file)
        {
            using (var fileStream = await file.OpenAsync(FileAccessMode.Read))
            {
                var compressedStream = fileStream.AsStreamForRead();
                var deserializer = new XmlSerializer(typeof(DtObjectsCollection));

                using (var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
                {
                    var data = deserializer.Deserialize(zipStream) as DtObjectsCollection;

                    return (data.Data);
                }
            }
        }
#else
        private Task<List<DtMosToilet>> ReadToiletsDataFromXmlZipAsync(string file)
        {
            var token = new Task<List<DtMosToilet>>(() =>
            {
                using (var fileStream = Application.GetResourceStream(new Uri(file, UriKind.Relative)).Stream)
                {
                    var compressedStream = fileStream;
                    var deserializer = new XmlSerializer(typeof(DtObjectsCollection));

                    using (var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
                    {
                        var data = deserializer.Deserialize(zipStream) as DtObjectsCollection;

                        return (data.Data);
                    }
                }
            });
            token.Start();

            return (token);
        }
#endif

#if WINDOWS_STORE
        private async Task<List<DtMosToilet>> ReadToiletsDataFromXmlAsync(StorageFile file)
        {
            using (var fileStream = await file.OpenAsync(FileAccessMode.Read))
            {
                var stream = fileStream.AsStreamForRead();
                var deserializer = new XmlSerializer(typeof(DtObjectsCollection));

                var data = deserializer.Deserialize(stream) as DtObjectsCollection;

                return (data.Data);
            }
        }
#else
        private Task<List<DtMosToilet>> ReadToiletsDataFromXmlAsync(string file)
        {
            var token = new Task<List<DtMosToilet>>(() =>
                {
                    using (var fileStream = IsolatedStorageFile.GetUserStoreForApplication().OpenFile(file, FileMode.Open))
                    {
                        var stream = fileStream;
                        var deserializer = new XmlSerializer(typeof(DtObjectsCollection));

                        var data = deserializer.Deserialize(stream) as DtObjectsCollection;

                        return (data.Data);
                    } 
                });
            token.Start();

            return (token);
        }
#endif

#if WINDOWS_STORE
        private async Task SaveToiletsDataFromXmlAsync(StorageFile file, List<DtMosToilet> data)
        {
            using (var stream = await file.OpenAsync(FileAccessMode.ReadWrite))
            {
                var streamForWrite = stream.AsStreamForWrite();

                var collection = new DtObjectsCollection() { Data = data };
                var serializer = new XmlSerializer(typeof(DtObjectsCollection));
                serializer.Serialize(streamForWrite, collection);

                await streamForWrite.FlushAsync();
            }
        }
#else
        private Task SaveToiletsDataFromXmlAsync(string file, List<DtMosToilet> data)
        {
            var token = new Task(() =>
            {
                using (var stream = IsolatedStorageFile.GetUserStoreForApplication().OpenFile(file, FileMode.OpenOrCreate))
                {
                    var streamForWrite = stream;

                    var collection = new DtObjectsCollection() { Data = data };
                    var serializer = new XmlSerializer(typeof(DtObjectsCollection));
                    serializer.Serialize(streamForWrite, collection);

                    streamForWrite.Flush();
                }
            });
            token.Start();

            return (token);
        }
#endif

        private string Type2FileName(StorageType type)
        {
            switch (type)
            {
                case StorageType.OfficialDataMoscow:
                    return ("moscow_toilets_geo.xml");
                case StorageType.OpenData:
                    return ("people_choice_toilets_geo.xml");
                case StorageType.UserData:
                    return ("user_toilets_geo.xml");
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }

#if WINDOWS_STORE
        internal async Task<List<DtMosToilet>> GetDataSource(StorageType type)
        {
            var fileName = Type2FileName(type);
            var cacheFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(FILE_CACHE_FOLDER, 
                CreationCollisionOption.OpenIfExists);
            
            try
            {
                var file = await cacheFolder.GetFileAsync(fileName);
                var data = await ReadToiletsDataFromXmlAsync(file);

                return (data);
            }
            catch (Exception ex)
            {     
            }

            var resourceFile = await StorageFile.GetFileFromApplicationUriAsync(
                new Uri("ms-appx:///Resources/Data/" + fileName + ".gz"));
            var resourceData = await ReadToiletsDataFromXmlZipAsync(resourceFile);

            var cacheFile = await cacheFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
            await SaveToiletsDataFromXmlAsync(cacheFile, resourceData);

            return (resourceData);
        }
#else
        internal async Task<List<DtMosToilet>> GetDataSource(StorageType type)
        {
            var filePath = FILE_CACHE_FOLDER + "/" + Type2FileName(type);

            try
            {
                var data = await ReadToiletsDataFromXmlAsync(filePath);

                return (data);
            }
            catch (Exception ex)
            {
            }

            var token = new Task(() => IsolatedStorageFile.GetUserStoreForApplication().CreateDirectory(FILE_CACHE_FOLDER));
            token.Start();
            await token;

            var resourceData = await ReadToiletsDataFromXmlZipAsync("Resources/Data/" + Type2FileName(type) + ".gz");
            await SaveToiletsDataFromXmlAsync(filePath, resourceData);

            return (resourceData);
        }
#endif

#if WINDOWS_PHONE
        internal async void AddToiletAsync(StorageType type, DtMosToilet toilet)
        {
            var filePath = FILE_CACHE_FOLDER + "/" + Type2FileName(type);
        }
#endif
    }
}
