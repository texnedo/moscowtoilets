﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoscowToilets.Model.DataMos
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class DtMosDataContract : System.Attribute
    {
        private readonly string _name;

        public DtMosDataContract(string name)
        {
            _name = name;
        }

        public string Name
        {
            get { return _name; }
        }
    }

    internal class DtMosObjectParseException : System.Exception
    {
        private string _errorLine = "";

        internal string ErrorLine { get { return (_errorLine); } }

        internal DtMosObjectParseException(string csvLine)
        {
            _errorLine = csvLine;
        }
    }
}
