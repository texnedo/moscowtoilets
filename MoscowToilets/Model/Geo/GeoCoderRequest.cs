﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using GeoAPI.Geometries;

#if WINDOWS_STORE
using Windows.Foundation;
using System.Runtime.Serialization.Json;
#endif

namespace MoscowToilets.Model.Geo
{
#if WINDOWS_STORE
    internal class GeoCoderRequest
    {
        private readonly Uri _targetUrl;

        private const string BaseUrlFormat = "http://geocode-maps.yandex.ru/1.x/?geocode={0}&format=json";

        private readonly DataContractJsonSerializer StoreContentResponseSerializer = new DataContractJsonSerializer(typeof(GeocoderResponce));

        private GeocoderResponce ReadContentReseponse(string response)
        {
            try
            {
                var ms = new MemoryStream(Encoding.Unicode.GetBytes(response));
                ms.Seek(0, SeekOrigin.Begin);
                var storeContent = (GeocoderResponce)StoreContentResponseSerializer.ReadObject(ms);
                return storeContent;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public GeoCoderRequest(string address)
        {
            _targetUrl = new Uri(string.Format(BaseUrlFormat, address));
        }

        internal Task<GeocoderResponce> ExecuteAsync()
        {
            var wc = WebRequest.Create(_targetUrl);

            return (wc.GetResponseAsync()).ContinueWith((data) => {

                var dataStream = data.Result.GetResponseStream();
                var reader = new StreamReader(dataStream);
                
                return (ReadContentReseponse(reader.ReadToEnd())); 
            });
        }

        internal Task<Coordinate> GetCoordinateAsync()
        {
            return (ExecuteAsync()).ContinueWith((responce) => {
                    
                var coord = new Coordinate()
                    {
                        X = responce.Result.Responce.Collection.Items.First().Object.GeoPoint.Lattitude,
                        Y = responce.Result.Responce.Collection.Items.First().Object.GeoPoint.Longitude
                    };

                return (coord);
            });
        }
    }

    [DataContract]
    public class GeocoderResponce
    {
        [DataContract]
        public class GeocodeResponceInternal
        {
            [DataMember(Name = "GeoObjectCollection")]
            public GeoObjectCollection Collection;
        }

        [DataMember(Name = "response")]
        public GeocodeResponceInternal Responce { get; set; }
    }

    [DataContract]
    public class GeoObjectCollection
    {
        [DataContract]
        public class GeoObjectInternal
        {
            [DataMember(Name = "GeoObject")]
            public GeoObject Object;
        }

        [DataMember(Name = "metaDataProperty")]
        public GeocoderResponseMetaData MetaData { get; set; }

        [DataMember(Name = "featureMember")]
        public List<GeoObjectInternal> Items { get; set; }
    }

    [DataContract]
    public class GeocoderResponseMetaData
    {

    }

    [DataContract]
    public class GeoObjectMetadata
    {

    }

    [DataContract]
    public class Envelope
    {
        [DataMember(Name = "lowerCorner")]
        public string LowerCorner { get; set; }

        [DataMember(Name = "upperCorner")]
        public string UpperCorner { get; set; }
    }

    [DataContract]
    public class Point
    {
        private string[] _pointData;

        private string _position;

        [DataMember(Name = "pos")]
        public string Position
        {
            get { return (_position); }
            set
            {
                _position = value;
                if (!string.IsNullOrEmpty(_position))
                    _pointData = _position.Split(new char[] { ' ' });
                else
                    System.Diagnostics.Debug.Assert(false);
            }
        }

        internal double Lattitude
        {
            get
            {
                if (_pointData != null && _pointData.Length == 2)
                    return (Convert.ToDouble(_pointData[1]));
                else
                {
                    System.Diagnostics.Debug.Assert(false);
                    return (0);
                }
            }
        }

        internal double Longitude
        {
            get
            {
                if (_pointData != null && _pointData.Length == 2)
                    return (Convert.ToDouble(_pointData[0]));
                else
                {
                    System.Diagnostics.Debug.Assert(false);
                    return (0);
                }
            }
        }
    }

    [DataContract]
    public class GeoObject
    {
        [DataContract]
        public class EnvelopeInternal
        {
            [DataMember(Name = "Envelope")]
            public Envelope Bounaries { get; set; }
        }

        [DataMember(Name = "metaDataProperty")]
        public GeoObjectMetadata Metadata { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "boundedBy")]
        public EnvelopeInternal BoundedBy { get; set; }

        [DataMember(Name = "Point")]
        public Point GeoPoint { get; set; }
    }
#endif
}
