﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GeoCoordinate = Yandex.Positioning.GeoCoordinate;

#if WINDOWS_STORE
    using Windows.Devices.Geolocation;
    using Windows.UI.Core;
#elif WINDOWS_PHONE
#if WP7
    using MoscowToiletsWP7;
#elif WP8
    using MoscowToiletsWP8;
#endif
    using Microsoft.Phone.Reactive;
    using System.Device.Location;
    using System.Windows;
    using System.Windows.Threading;
#endif

namespace MoscowToilets.Model.Geo
{
    enum GeoLocationProviderState
    {
        PositionUnavalible = 0,
        PositionCached = 1,
        LookingForPosition = 2,
        PositionAvalible = 3,
        PositionDisabled = 4
    }

    internal class GeoPositionDescriptor
    {
        public GeoCoordinate Position { get; set; }

        public GeoLocationProviderState State { get; set; }
    }

    internal class GeoLocationProvider : ObservableObject
    {
#if WINDOWS_STORE
        private readonly Geolocator _geolocator = new Geolocator();
#elif WINDOWS_PHONE
        private readonly GeoCoordinateWatcher _geolocator = new GeoCoordinateWatcher();
#endif
        private GeoCoordinate _lastPosition = null;

        private GeoCoordinate LastPosition
        {
            get { return _lastPosition; }
            set
            {
                _lastPosition = value;
                if (value != null)
                    App.Settings.SetValue("GeoLocationProvider.LastPosition", value.ToString());
            }
        }

#if WINDOWS_STORE
        private readonly CoreDispatcher _dispatcher = null;
#elif WINDOWS_PHONE
        private readonly Dispatcher _dispatcher = null;
#endif

        internal GeoLocationProvider()
        {
            if (App.Settings.HasValue("GeoLocationProvider.LastPosition"))
                LastPosition = new GeoCoordinate((string)App.Settings.GetValue("GeoLocationProvider.LastPosition"));

            _geolocator.PositionChanged += _geolocator_PositionChanged;
            _geolocator.StatusChanged += _geolocator_StatusChanged;

            App.AppActivationChanged += (obj, args) =>
                {
                    if (args.Acvivated)
                    {
                        _geolocator_StatusChanged(this, new GeoPositionStatusChangedEventArgs(_geolocator.Status));
                    }
                };

#if WINDOWS_STORE
            _dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
#elif WINDOWS_PHONE
            _dispatcher = Deployment.Current.Dispatcher;
            _geolocator.Start();
#endif
        }

#if WINDOWS_STORE
        private void _geolocator_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                switch (args.Status)
                {
                    case PositionStatus.Ready:
                        {
                            LocationState = new GeoPositionDescriptor()
                            {
                                Position = LastPosition,
                                State = GeoLocationProviderState.LookingForPosition
                            };
                            _geolocator.GetGeopositionAsync().AsTask().ContinueWith((result) =>
                            {
                                try
                                {
                                    var geoPosition = new GeoCoordinate(result.Result.Coordinate.Latitude,
                                                                    result.Result.Coordinate.Longitude);
                                    LastPosition = geoPosition;
                                    LocationState = new GeoPositionDescriptor()
                                    {
                                        Position = geoPosition,
                                        State = GeoLocationProviderState.PositionAvalible
                                    };
                                }
                                catch (Exception ex)
                                {
                                    LocationState = new GeoPositionDescriptor()
                                    {
                                        Position = LastPosition,
                                        State = GeoLocationProviderState.PositionUnavalible
                                    };
                                }

                            }, TaskScheduler.FromCurrentSynchronizationContext());
                            break;
                        }
                    case PositionStatus.Initializing:
                        {
                            LocationState = new GeoPositionDescriptor()
                            {
                                Position = null,
                                State = GeoLocationProviderState.LookingForPosition
                            };
                            break;
                        }
                    case PositionStatus.NotInitialized:
                    case PositionStatus.NoData:
                    case PositionStatus.NotAvailable:
                        {
                            LocationState = new GeoPositionDescriptor()
                            {
                                Position = LastPosition,
                                State = GeoLocationProviderState.PositionUnavalible
                            };
                            break;
                        }
                    case PositionStatus.Disabled:
                        {
                            LocationState = new GeoPositionDescriptor()
                            {
                                Position = null,
                                State = GeoLocationProviderState.PositionDisabled
                            };
                            break;
                        }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            });
        }
#elif WINDOWS_PHONE
        private void _geolocator_StatusChanged(object sender, GeoPositionStatusChangedEventArgs args)
        {
            _dispatcher.BeginInvoke(() =>
            {
                switch (args.Status)
                {
                    case GeoPositionStatus.Ready:
                        {
                            LocationState = new GeoPositionDescriptor()
                            {
                                Position = LastPosition,
                                State = GeoLocationProviderState.LookingForPosition
                            };
                            break;
                        }
                    case GeoPositionStatus.Initializing:
                        {
                            LocationState = new GeoPositionDescriptor()
                            {
                                Position = null,
                                State = GeoLocationProviderState.LookingForPosition
                            };
                            break;
                        }
                    case GeoPositionStatus.NoData:

                        {
                            LocationState = new GeoPositionDescriptor()
                            {
                                Position = LastPosition,
                                State = GeoLocationProviderState.PositionCached
                            };
                            break;
                        }
                    case GeoPositionStatus.Disabled:
                        {
                            LocationState = new GeoPositionDescriptor()
                            {
                                Position = null,
                                State = GeoLocationProviderState.PositionDisabled
                            };
                            break;
                        }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            });
        }
#endif

#if WINDOWS_STORE
        private void _geolocator_PositionChanged(Geolocator sender, Windows.Devices.Geolocation.PositionChangedEventArgs args)
        {
            _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    var geoPosition = new GeoCoordinate(args.Position.Coordinate.Latitude, 
                        args.Position.Coordinate.Longitude);
                    LastPosition = geoPosition;
                    LocationState = new GeoPositionDescriptor()
                    {
                        Position = geoPosition,
                        State = GeoLocationProviderState.PositionAvalible
                    };
                });
        }
#elif WINDOWS_PHONE
        private void _geolocator_PositionChanged(object sender, GeoPositionChangedEventArgs<System.Device.Location.GeoCoordinate> args)
        {
            _dispatcher.BeginInvoke(() =>
            {
                var geoPosition = new GeoCoordinate(args.Position.Location.Latitude,
                    args.Position.Location.Longitude);
                LastPosition = geoPosition;
                LocationState = new GeoPositionDescriptor()
                {
                    Position = geoPosition,
                    State = GeoLocationProviderState.PositionAvalible
                };
            });
        }
#endif

        private GeoPositionDescriptor SwitchToState(GeoPositionDescriptor state)
        {
            if (_locationState == null)
                return (state);

            switch (_locationState.State)
            {
                case GeoLocationProviderState.PositionUnavalible:
                case GeoLocationProviderState.PositionCached:
                case GeoLocationProviderState.LookingForPosition:
                case GeoLocationProviderState.PositionDisabled:
                    return (state);
                case GeoLocationProviderState.PositionAvalible:
                    {
                        switch (state.State)
                        {
                            case GeoLocationProviderState.PositionUnavalible:
                            case GeoLocationProviderState.PositionDisabled:
                            case GeoLocationProviderState.PositionAvalible:
                                return (state);
                            case GeoLocationProviderState.PositionCached:
                            case GeoLocationProviderState.LookingForPosition:
                                return (_locationState);
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private GeoPositionDescriptor _locationState;

        public GeoPositionDescriptor LocationState
        {
            get 
            { 
                if (_locationState == null)
                {
                    _locationState = new GeoPositionDescriptor
                        {
                            Position = _lastPosition,
                            State = GeoLocationProviderState.PositionUnavalible
                        };
                }

                return (_locationState); 
            }
            private set
            {
                _locationState = SwitchToState(value);
                RaisePropertyChanged("LocationState");
            }
        }
    }
}
