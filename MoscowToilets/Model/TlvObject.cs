﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if WINDOWS_STORE
    using Windows.Storage.Streams;
#elif WINDOWS_PHONE
    using System.IO.IsolatedStorage;
    using System.IO;
#endif

namespace MoscowToilets.Model
{
    /*Class represents entity providing functionality for saving data into binary destStream in TLV format.*/
    class TlvObject
    {
	    private Int16 _type;
	    
#if WINDOWS_STORE
#elif WINDOWS_PHONE
        private readonly MemoryStream _stream = new MemoryStream();

        private BinaryWriter _dataWriter;
#endif

        public TlvObject( Int16 type )
        {
            _type = type;
            _dataWriter = new BinaryWriter(_stream);
            _dataWriter.Write(_type);
        }

        public TlvObject( Int16 type, Int32 iData )
        {
            _type = type;
            _dataWriter = new BinaryWriter(_stream);
            _dataWriter.Write(_type);
            _dataWriter.Write(sizeof(Int32));
            _dataWriter.Write(iData);
            
        }

        public TlvObject( Int16 type, Int64 iData)
        {
            _type = type;
            _dataWriter = new BinaryWriter(_stream);
            
                _dataWriter.Write(_type);
                _dataWriter.Write(sizeof(Int32));
                _dataWriter.Write(iData);
            
        }

        public TlvObject( Int16 type, string sData)
        {
            if (string.IsNullOrEmpty(sData))
            {
                System.Diagnostics.Debug.Assert(false);
                return;
            }

            _type = type;

            var dataWriter = new BinaryWriter(_stream);
            
            var data = Encoding.UTF8.GetBytes(sData);
            dataWriter.Write(data.Length);
            dataWriter.Write(data);
            
        }

        public TlvObject( Stream stream )
        {
            Load(stream);
        }

        public void PushTLV( TlvObject tlv )
        {
            if (tlv == null)
            {
                System.Diagnostics.Debug.Assert(false);
                return;
            }

            tlv._stream.Seek(0, SeekOrigin.Begin);
            tlv._stream.CopyTo(_stream);
        }

        private void Load( Stream srcStream )
        {
            using (var streamReader = new BinaryReader(srcStream))
            {
                _type = streamReader.ReadInt16();
                var tlvSize = streamReader.ReadInt32();
                _dataWriter = new BinaryWriter(_stream);

                _dataWriter.Write(streamReader.ReadBytes(tlvSize));
                    
            }
        }

        public void Save( Stream destStream )
        {
            _stream.Seek(0, SeekOrigin.Begin);

            _dataWriter = new BinaryWriter(_stream);
            
            _dataWriter.Write(_type);
            _dataWriter.Write(_stream.Length);
            

            _stream.CopyTo(destStream);
        }


        /*
        public TlvObject PopTLV()
        {
            if (!_dataWriter)
                return (TlvObject());

            if (!_dataReader)
                _dataReader =  DataReader::FromBuffer(_dataWriter.DetachBuffer());

            if (!_dataReader.UnconsumedBufferLength)
                return (TlvObject());

            try
            {
                //read type of nested tlv
                Int32 tlvType = _dataReader.ReadInt32();

                //read size of nested tlv
                int tlvSize = _dataReader.ReadInt32();

                TlvObject readTlv(new TlvObject(tlvType));

                Platform::Array<BYTE>^ tmpBuffer = ref new Platform::Array<BYTE>(tlvSize);
                _dataReader.ReadBytes(tmpBuffer);

                readTlv.m_rwData.WriteBytes(tmpBuffer);

                return (readTlv);
            }
            catch (Platform::Exception^ e)
            {
		
            }

            return (TlvObject());
        }

        bool GetDataString( MAKFC_CString& sData )
        { 
            if (!_dataWriter)
                return (false);

            if (!_dataReader)
                _dataReader = DataReader::FromBuffer(_dataWriter.DetachBuffer());

            if (!_dataReader.UnconsumedBufferLength)
            {
                sData = L"";
                return (true);
            }

            try
            {
                Platform::Array<BYTE>^ stringData = ref new Platform::Array<BYTE>( _dataReader.UnconsumedBufferLength );
                _dataReader.ReadBytes(stringData);
 		
                MAKFC_CString sResultData((const WCHAR*)&stringData[0], stringData.Length / sizeof(WCHAR));
                sData = sResultData;

                return (true);
            }
            catch (Platform::Exception^ e)
            {

            }

            return (false);
        }

        bool GetData64( Int64& iData )
        {
            if (!_dataWriter)
                return (false);

            if (!_dataReader)
                _dataReader =  DataReader::FromBuffer(_dataWriter.DetachBuffer());

            if (!_dataReader.UnconsumedBufferLength)
                return (false);

            try
            {
                iData = _dataReader.ReadInt64();

                return (true);
            }
            catch (Platform::Exception^ e)
            {

            }

            return (false);
        }

        bool GetData32( Int32& iData )
        {
            if (!_dataWriter)
                return (false);

            if (!_dataReader)
                _dataReader =  DataReader::FromBuffer(_dataWriter.DetachBuffer());

            if (!_dataReader.UnconsumedBufferLength)
                return (false);

            try
            {
                iData = _dataReader.ReadInt32();

                return (true);
            }
            catch (Platform::Exception^ e)
            {

            }

            return (false);
        }

        void Save( DataWriter^ rw )
        {
            if (!_dataWriter)
                return;

            rw.Write(_type);

            IBuffer^ dataBuffer = _dataWriter.DetachBuffer();
            rw.Write(dataBuffer.Length);
            rw.WriteBuffer(dataBuffer);
        }

        bool Load( DataReader^ rw )
        {
            if (!_dataWriter)
                _dataWriter = ref new DataWriter();

            if (!rw.UnconsumedBufferLength)
                return (false);

            try
            {
                _type = rw.ReadInt32();

                int tlvSize = rw.ReadInt32();

                Platform::Array<BYTE>^ tmpBuffer = ref new Platform::Array<BYTE>(tlvSize);
                rw.ReadBytes(tmpBuffer);

                _dataWriter.WriteBytes(tmpBuffer);

                return (true);
            }
            catch (Platform::Exception^ e)
            {
		
            }

            return (false);
        }

        void PushTLVDataString( Int32 type, const MAKFC_CString& sData )
        {
            if (!_dataWriter)
                _dataWriter = ref new DataWriter();

            //write string content
            _dataWriter.Write(type);
            auto data = ref new Platform::Array<BYTE>((BYTE*)(LPCWSTR)sData, sData.NetSizeW());
            _dataWriter.Write(data.Length);
            _dataWriter.WriteBytes(data);
        }

        void PushTLVData32( Int32 type, Int32 iData )
        {
            if (!_dataWriter)
                _dataWriter = ref new DataWriter();

            _dataWriter.Write(type);
            _dataWriter.Write(sizeof(iData));
            _dataWriter.Write(iData);
        }

        void PushTLVData64( Int32 type, Int64 iData )
        {
            if (!_dataWriter)
                _dataWriter = ref new DataWriter();

            _dataWriter.Write(type);
            _dataWriter.Write(sizeof(iData));
            _dataWriter.Write(iData);
        }*/

    }
}
