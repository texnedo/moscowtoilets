﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

#if WINDOWS_STORE
using Windows.UI.Xaml;
#elif WINDOWS_PHONE
using System.Windows;
#endif

namespace MoscowToilets.View
{
#if WINDOWS_STORE
    [Windows.UI.Xaml.Data.Bindable]
#endif
    public class ViewCommandAdapter : DependencyObject, INotifyPropertyChanged
    {
        private static readonly DependencyProperty _targetCommandProperty = DependencyProperty.Register(
                                                        "TargetCommand",
                                                        typeof(ICommand),
                                                        typeof(ViewCommandAdapter),
                                                        null
                                                        );

        public ICommand TargetCommand
        {
            get { return ((ICommand)GetValue(_targetCommandProperty)); }

            set
            {
                SetValue(_targetCommandProperty, value);
                RaisePropertyChanged("TargetCommand");
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        internal void Execute()
        {
            if (TargetCommand != null && TargetCommand.CanExecute(null))
                TargetCommand.Execute(null);
        }
    }
}
