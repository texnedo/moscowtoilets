﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if WINDOWS_STORE
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
#elif WINDOWS_PHONE
using System.Windows;
using System.Windows.Data;
#endif

namespace MoscowToilets.View
{
    public class SwitchOnOffConverter : IValueConverter
    {
#if WINDOWS_STORE
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((bool)value) ? "Да" : "Нет";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
#elif WINDOWS_PHONE
        public object Convert(object value, Type targetType, object parameter, CultureInfo info)
        {
            return ((bool)value) ? "Да" : "Нет";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo info)
        {
            throw new NotImplementedException();
        }
#endif
    }
}
