﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using Yandex.Maps.Events;
using Yandex.Positioning;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MoscowToilets.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }

        private void Map_OnTapped(object sender, TappedRoutedEventArgs e)
        {
        }

        private void Map_OnOperationStatusChanged(object sender, OperationStatusChangedEventArgs e)
        {
        }

        private void ZoomInButton_OnClick(object sender, RoutedEventArgs e)
        {
            map.ZoomIn();
        }

        private void ZoomOutButton_OnClick(object sender, RoutedEventArgs e)
        {
            map.ZoomOut();
        }
    }
}
