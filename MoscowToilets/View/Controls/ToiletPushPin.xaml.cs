﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Yandex.Maps;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MoscowToilets.View.Controls
{
    public sealed partial class ToiletPushPin : PushPin
    {
        private readonly Popup _popup = new Popup();

        public ToiletPushPin()
        {
            this.InitializeComponent();

            _popup.IsLightDismissEnabled = true;
            _popup.Closed += (obj, args) => selectedAdapter.Execute();

            Loaded += (obj, args) => { _popup.IsOpen = false; };
            Unloaded += (obj, args) => { _popup.IsOpen = false; };
        }

        private void ContentGrid_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            selectedAdapter.Execute();

            var left = TransformToVisual(Window.Current.Content).TransformPoint(new Windows.Foundation.Point(0, 0)).X;
            var top = TransformToVisual(Window.Current.Content).TransformPoint(new Windows.Foundation.Point(0, 0)).Y;

            var flyout = new ContentControl
                {
                    Template = Resources["FlyoutTemplate"] as ControlTemplate,
                    DataContext = DataContext
                };

            _popup.Child = flyout;
            _popup.Margin = new Thickness(15, 15, 0, 0);
            _popup.SetValue(Canvas.LeftProperty, left);
            _popup.SetValue(Canvas.TopProperty, top);
            _popup.IsOpen = true;
        }
    }
}
