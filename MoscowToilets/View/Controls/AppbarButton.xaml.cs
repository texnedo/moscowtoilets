﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using GalaSoft.MvvmLight;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MoscowToilets.View.Controls
{
    [Windows.UI.Xaml.Data.Bindable]
    public sealed partial class AppbarButton : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _pressed;
		
        private string _normalIconPath;
		
        private string _pressedIconPath;
		
        private string _tempNormalIconPath;

        private static readonly DependencyProperty _normalIconSourceProperty = DependencyProperty.Register(
                                                                "NormalIconSource",
                                                                typeof(BitmapImage),
                                                                typeof(AppbarButton),
                                                                null
                                                                );

        public string NormalIconPath
        {
            get { return _normalIconPath; }

            set
			{
				_normalIconPath = value;
                SetValue(_normalIconSourceProperty,
			             new BitmapImage(new Uri(value)));
				RaisePropertyChanged("NormalIconSource");
			}
        }

        private static readonly DependencyProperty _pressedIconSourceProperty = DependencyProperty.Register(
                                                        "PressedIconSource",
                                                        typeof(BitmapImage),
                                                        typeof(AppbarButton),
                                                        null
                                                        );

        public string PressedIconPath
        {
            get { return _pressedIconPath; }

            set
            {
                _pressedIconPath = value;
                SetValue(_pressedIconSourceProperty,
                         new BitmapImage(new Uri(value)));
                RaisePropertyChanged("PressedIconSource");
            }
        }

        private static readonly DependencyProperty _labelTextProperty = DependencyProperty.Register(
                                                "LabelText",
                                                typeof(string),
                                                typeof(AppbarButton),
                                                null
                                                );

        public string LabelText
		{
			set
			{
				SetValue(_labelTextProperty, value);
				RaisePropertyChanged("LabelText");
			}
            get { return (string) (GetValue(_labelTextProperty)); }
		}

        public ImageSource NormalIconSource
		{
            get { return (ImageSource) (GetValue(_normalIconSourceProperty)); }
		}

		public ImageSource PressedIconSource
		{
            get{return(ImageSource) (GetValue(_pressedIconSourceProperty));}
		}

        public string TempNormalIconPath
        {
            get { return _tempNormalIconPath; }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

		public bool IsChecked
		{
			get { return (_pressed); }
			
            set 
			{ 
				_pressed = value; 
				RaisePropertyChanged("IsChecked");
			}
		}

        public static readonly DependencyProperty CheckBoxStyleProperty = DependencyProperty.Register(
                                                                        "CheckBoxStyle", 
                                                                        typeof(Boolean),
                                                                        typeof(AppbarButton), 
                                                                        null
                                                                        );
        public bool CheckBoxStyle //the property wrapper
        {
            get { return (bool)GetValue(CheckBoxStyleProperty); }

            set { SetValue(CheckBoxStyleProperty, value); }
        }

        public static DependencyProperty NormalIconSourceProperty
        {
            get { return _normalIconSourceProperty; }
        }

        public AppbarButton()
        {
            this.InitializeComponent();
            this.Click += AppbarButton_Click;
        }

        void AppbarButton_Click(object sender, RoutedEventArgs e)
        {
            _pressed = !_pressed;

            if (CheckBoxStyle)
            {
                if (_pressed)
                {
                    _tempNormalIconPath = _normalIconPath;
                    NormalIconPath = PressedIconPath;
                }
                else
                {
                    NormalIconPath = _tempNormalIconPath;
                }
            }
        }
    }
}
