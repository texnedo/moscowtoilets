﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace MoscowToiletsWP7.View.Pages
{
    public partial class ToitetDescriptionPage : PhoneApplicationPage
    {
        public ToitetDescriptionPage()
        {
            InitializeComponent();
        }

        private void BackButton_OnClick(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void ViewImage_OnClick(object sender, EventArgs e)
        {
            showBigImageCommand.Execute();
        }
    }
}