﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Coding4Fun.Toolkit.Controls;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Phone.Controls;
using MoscowToiletsWP7.View.Controls;
using Yandex.Maps;
using Yandex.Maps.Events;
using Yandex.Positioning;
using System.Diagnostics;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace MoscowToiletsWP7.View.Pages
{
    public class TextChangeEventArgs : EventArgs
    {
        public string Value { get; set; }
    }

    public class TextChangeConverter : IValueConverter
    {
        private string _lastValue;

        public event EventHandler<TextChangeEventArgs> TextChanged;

        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (TextChanged != null && _lastValue != value)
                TextChanged(this, new TextChangeEventArgs() {Value = value.ToString()});
            
            _lastValue = value.ToString();
            return (value);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            throw (new NotImplementedException());
        }
    }

    public partial class MainPage : PhoneApplicationPage
    {
        private readonly DispatcherTimer _statusVisibilityTimer = new DispatcherTimer();

        private bool _geoStatusBarVisible = false;

        public MainPage()
        {
            InitializeComponent();
            ApplicationBar.Opacity = 0.7;

            Messenger.Default.Register<string>(this, (action) =>
                {
                    if (action == "ShowGeoStatus")
                    {
                        ShowGeoStatusBar();
                    }
                });
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            _statusVisibilityTimer.Stop();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _statusVisibilityTimer.Interval = TimeSpan.FromSeconds(10);
            _statusVisibilityTimer.Tick += (obj, args) => HideStatusBar();

            var textConv = this.Resources["statusTextConv"] as TextChangeConverter;
            if (textConv != null)
                textConv.TextChanged += (obj, args) => ShowGeoStatusBar();

            if (_geoStatusBarVisible)
                _statusVisibilityTimer.Start();

            ajustCollectionAdapter.Execute();
        }

        private void Map_OnOperationStatusChanged(object sender, OperationStatusChangedEventArgs e)
        {
            switch (e.OperationStatus)
            {
                case OperationStatus.Idle:
                    Debug.WriteLine("Map OperationStatus = Idle, ViewPort = " + map.Viewport.ToString());
                    break;
                case OperationStatus.Normal:
                    Debug.WriteLine("Map OperationStatus = Normal, ViewPort = " + map.Viewport.ToString());
                    break;
                case OperationStatus.Busy:
                    Debug.WriteLine("Map OperationStatus = Busy, ViewPort = " + map.Viewport.ToString());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (map.Viewport != CurrentViewport)
                CurrentViewport = map.Viewport;
        }

        private void ZoomIn_OnClick(object sender, EventArgs e)
        {
            map.ZoomIn();
        }

        private void ZoomOut_OnClick(object sender, EventArgs e)
        {
            map.ZoomOut();
        }

        private void Filter_OnClick(object sender, EventArgs e)
        {
            var popup = new ChooseFilterPrompt() { DataContext = DataContext };
            popup.Show();
        }

        private Yandex.Media.Rect _currentViewport;

        public Yandex.Media.Rect CurrentViewport
        {
            get { return (_currentViewport); }
            set 
            {
                _currentViewport = value;
                ajustCollectionAdapter.Execute();
            }
        }

        public bool IsCoordinateVisibleForUser(GeoCoordinate coordinate)
        {
            var point = map.CoordinatesToViewportPoint(coordinate);

            if (point.X > map.Viewport.Left && point.X < map.Viewport.Right && point.Y > map.Viewport.Top &&
                point.Y < map.Viewport.Bottom)
                return (true);
            else
                return (false);
        }

        private void HideStatusBar()
        {
            if (!_geoStatusBarVisible)
                return;

            _geoStatusBarVisible = false;

            var board = new Storyboard();
            var animation = new DoubleAnimation();
            Storyboard.SetTarget(animation, statuBarGrid);
            Storyboard.SetTargetProperty(animation, new PropertyPath("(UIElement.RenderTransform).(TranslateTransform.Y)"));
            animation.From = 0;
            animation.To = -40;
            animation.SpeedRatio = 2.0;
            board.Children.Add(animation);

            board.Begin();

            if (_statusVisibilityTimer.IsEnabled)
                _statusVisibilityTimer.Stop();
        }

        private void ShowGeoStatusBar()
        {
            if (_geoStatusBarVisible)
                return;

            _geoStatusBarVisible = true;

            var board = new Storyboard();
            var animation = new DoubleAnimation();
            Storyboard.SetTarget(animation, statuBarGrid);
            Storyboard.SetTargetProperty(animation, new PropertyPath("(UIElement.RenderTransform).(TranslateTransform.Y)"));
            animation.From = -40;
            animation.To = 0;
            animation.SpeedRatio = 2.0;
            board.Children.Add(animation);

            board.Begin();

            if (_statusVisibilityTimer.IsEnabled)
                _statusVisibilityTimer.Stop();

            _statusVisibilityTimer.Start();
        }

        private void StatuBarGrid_OnTap(object sender, GestureEventArgs e)
        {
            HideStatusBar();
        }

        private void AddToilet_OnClick(object sender, EventArgs e)
        {
            addNewToiletAdapter.Execute();
        }

        private void About_OnClick(object sender, EventArgs e)
        {
            openAboutPageAdapter.Execute();
        }

        private void Settings_OnClick(object sender, EventArgs e)
        {
            settingsAdapter.Execute();
        }
    }
}