﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace MoscowToiletsWP7.View.Pages
{
    public partial class AboutPage : PhoneApplicationPage
    {
        public AboutPage()
        {
            InitializeComponent();

            var nameHelper = new AssemblyName(Assembly.GetExecutingAssembly().FullName);
            VersionText.Text += nameHelper.Version;
        }

        private void MailToLink_OnClick(object sender, RoutedEventArgs e)
        {
            var emailComposeTask = new EmailComposeTask();

            emailComposeTask.Subject = "WC Moscow";
            emailComposeTask.Body = "";
            emailComposeTask.To = "vnedash@mail.ru";

            emailComposeTask.Show();
        }
    }
}