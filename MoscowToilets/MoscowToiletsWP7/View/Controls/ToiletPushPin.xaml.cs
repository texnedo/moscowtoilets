﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Yandex.Maps;

namespace MoscowToiletsWP7.View.Controls
{
    public sealed partial class ToiletPushPin : PushPin
    {
        public ToiletPushPin()
        {
            InitializeComponent();
        }

        private void ContentGrid_OnTapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            selectedAdapter.Execute();

            var flyout = new ToiletDescriptionFlyout() {DataContext = DataContext};
            flyout.Completed += (obj, args) => selectedAdapter.Execute();
            flyout.Show();
        }
    }
}
