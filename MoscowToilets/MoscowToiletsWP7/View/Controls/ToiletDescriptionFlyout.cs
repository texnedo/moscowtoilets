﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Navigation;
using Clarity.Phone.Extensions;
using Coding4Fun.Toolkit.Controls;

namespace MoscowToiletsWP7.View.Controls
{
    class ToiletDescriptionFlyout : ActionPopUp<object, object>
    {
        public ToiletDescriptionFlyout()
        {
            DefaultStyleKey = typeof(ToiletDescriptionFlyout);
            AnimationType = DialogService.AnimationTypes.Swivel;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var backButton  = GetTemplateChild("BackButton") as Button;
            if (backButton != null) backButton.Click += (obj, args) => Hide();

            /*var detailsButton = GetTemplateChild("ShowDetails") as Button;
            if (detailsButton != null) detailsButton.Click += 
                (obj, args) =>
                    {
                        (App.Current as App).RootFrame.Navigate(new Uri("/View/Pages/ToitetDescriptionPage.xaml", UriKind.Relative));
                    };

            var photoGrid = GetTemplateChild("PhotoGrid") as Grid;
            if (photoGrid != null) photoGrid.Tap += (obj, args) => (App.Current as App).RootFrame.Navigate(new Uri("/View/Pages/BigImageViewerPage.xaml", UriKind.Relative));*/
        }
    }
}
