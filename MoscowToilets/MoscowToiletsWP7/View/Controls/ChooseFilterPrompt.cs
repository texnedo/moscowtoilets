﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Clarity.Phone.Extensions;
using Coding4Fun.Toolkit.Controls;

namespace MoscowToiletsWP7.View.Controls
{
    class ChooseFilterPrompt : ActionPopUp<object, object>
    {
        public ChooseFilterPrompt()
        {
            DefaultStyleKey = typeof(ChooseFilterPrompt);
            AnimationType = DialogService.AnimationTypes.Swivel;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var backButton = GetTemplateChild("BackButton") as Button;
            if (backButton != null) backButton.Click += (obj, args) => Hide();
        }
    }
}
