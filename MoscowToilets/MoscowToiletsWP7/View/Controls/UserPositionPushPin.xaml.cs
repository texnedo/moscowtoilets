﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Yandex.Maps;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace MoscowToiletsWP7.View.Controls
{
    public partial class UserPositionPushPin : PushPin
    {
        public UserPositionPushPin()
        {
            InitializeComponent();
        }

        private void PositionGrid_OnTap(object sender, GestureEventArgs e)
        {
            Messenger.Default.Send<string>("ShowGeoStatus");
        }
    }
}
