﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MoscowToilets.Core;
using MoscowToilets.Model;
using MoscowToilets.ViewModel;
using MoscowToiletsWP7.View.Pages;

namespace MoscowToiletsWP7
{
    public partial class App : Application
    {
        public class DetectCoordinateException : Exception
        {
            public Yandex.Positioning.GeoCoordinate Coordinate { get; set; }
        }

        public ViewNagationFrame RootFrame { get; private set; }

        private static readonly CommonSettings _settings = new CommonSettings();

        internal static CommonSettings Settings
        {
            get { return _settings; }
        }

        public class AppActivationEventArgs : EventArgs
        {
            public bool Acvivated { get; set; }
        }

        internal static ViewModelLocator Locator
        {
            get { return (App.Current.Resources["Locator"] as ViewModelLocator); }
        }

        public static event EventHandler<AppActivationEventArgs> AppActivationChanged;

        internal static bool IsCoordinateVisibleForUser(Yandex.Positioning.GeoCoordinate coordinate)
        {
            try
            {
                var currentPage = ((App)Application.Current).RootFrame.Content as MainPage;
                if (currentPage != null)
                    return (currentPage.IsCoordinateVisibleForUser(coordinate));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }

            return (false);
        }

        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {
            // Global handler for uncaught exceptions. 
            UnhandledException += Application_UnhandledException;

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

            // Show graphics profiling information while debugging.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // Display the current frame rate counters.
                Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode, 
                // which shows areas of a page that are handed off to GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Disable the application idle detection by setting the UserIdleDetectionMode property of the
                // application's PhoneApplicationService object to Disabled.
                // Caution:- Use this under debug mode only. Application that disables user idle detection will continue to run
                // and consume battery power when the user is not using the phone.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }

            TlvObject t1 = new TlvObject(0);
            TlvObject t2 = new TlvObject(1, "test");
            TlvObject t3 = new TlvObject(2, 10);
            TlvObject t5 = new TlvObject(3, 10);
            //t1.PushTLV(t2);
            t1.PushTLV(t3);
            t1.PushTLV(t5);

            MemoryStream stream = new MemoryStream();
            t1.Save(stream);

            stream.Seek(0, SeekOrigin.Begin);

            var t4 = new TlvObject(stream);
        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            if (AppActivationChanged != null)
                AppActivationChanged(this, new AppActivationEventArgs() {Acvivated = true});
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
            if (AppActivationChanged != null)
                AppActivationChanged(this, new AppActivationEventArgs() { Acvivated = false });

            _settings.Save();
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
            _settings.Save();
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new ViewNagationFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion
    }
}